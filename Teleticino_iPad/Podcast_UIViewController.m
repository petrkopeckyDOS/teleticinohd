//
//  Telegiornali_UIViewController.m
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Podcast_UIViewController.h"
#import "Common.h"
#import "TBXML.h"
#import "StreamingViews.h"
#import "AppRecord.h"

@interface Podcast_UIViewController (Private)

- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath;

@end

@implementation Podcast_UIViewController

@synthesize table,dataAccess,arrayDati,miaCell,loading, imageDownloadsInProgress;

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (BOOL)webView:(UIWebView*)mywebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType{
    if (UIWebViewNavigationTypeLinkClicked == navigationType){
        NSURL *url=[request URL];
        NSString *scheme = [[url scheme] lowercaseString];
        if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"]){
            [[UIApplication sharedApplication] openURL:url]; 
            return NO;
        }
        else {
            return YES;
        }
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    webView.hidden = NO;
    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('my_image').height;"];
    CGRect frame = CGRectMake(0, 361 + 50 - [height intValue], 320, [height intValue]);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [webView setFrame:frame];
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 366 - frame.size.height);
    [UIView commitAnimations];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    webView.hidden = YES;
    CGRect frame = CGRectMake(0, 411, 320, 50);
    [webView setFrame:frame];
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 366);
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    StatSendOperation *stats = [[StatSendOperation alloc] init];
    [stats sendStats];
    
	self.navigationItem.title = @"Replay programmi";
    
    [self.table setBackgroundColor:[UIColor clearColor]];
	
	dataAccess = [[DataAccess alloc] init];
	arrayDati = [[NSMutableArray alloc] init];
    
    self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)clearTable{
	arrayDati = nil;
	dataAccess = nil;
	NSArray *allDownloads = [imageDownloadsInProgress allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
	[imageDownloadsInProgress removeAllObjects];
}

-(void)loadTable{
	if (!arrayDati)
		arrayDati = [[NSMutableArray alloc] init];
	if (!dataAccess){
		[self.table setHidden:YES];
		dataAccess = [[DataAccess alloc] init];
		[dataAccess SetItemsReceivedSelector:self :@selector(ricaricaTabella:)];
	}
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	[self.table setHidden:YES];
	[self.loading startAnimating];
    
 	[self.navigationController setNavigationBarHidden:NO animated:NO];
    
    if (_moviePlayerView){
        self.moviePlayerView = nil;
    }
    
    if(arrayDati) {
        [arrayDati removeAllObjects];
    }
    
    [self clearTable];
    [self loadTable];
    
	[dataAccess GetTeleTicinoData:@"Podcast"];
}

#define ELEMENTI 7


-(void) ricaricaTabella:(NSMutableData *)data {
    
    if (!data){
        [self.table setHidden:YES];
        [self.loading stopAnimating];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Impossibile contattare il server, connettività limitata o assente. Verifica la tua connessione a Internet e riprova. Se il problema persiste attendi qualche tempo e riprova." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        return;
    }
	
	NSLog(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);

	arrayDati = [[NSMutableArray alloc] init];
	
	TBXML *tbxml = [TBXML tbxmlWithXMLData:data];
	NSMutableArray *temp = [Common XMLGetItemsBelowNodeName:tbxml.rootXMLElement NodeName:@"servizio"]; //programma

    AppRecord *appRecord = nil;
	int j = 0;
	int x = 0;

	for (int i = 0; i < [temp count]; i++) {
		if (j == 0)	{
			++x;
			if ([arrayDati count] < x) {
				appRecord = [[AppRecord alloc] init];	
			} else {
				appRecord = arrayDati[(x - 1)];
			}
		}
		if (j == 0) {
            appRecord.pubDate = ((ItemKeyValue *)temp[i]).nodeValue;
			
		}	
		if (j == 1) {
            appRecord.title = ((ItemKeyValue *)temp[i]).nodeValue;
		}
		if (j == 2) {
			appRecord.subtitle = ((ItemKeyValue *)temp[i]).nodeValue;
		}	
        if (j == 3) {
			appRecord.imageURLString = ((ItemKeyValue *)temp[i]).nodeValue;
		}
        if (j == 4) {
			appRecord.detailURLString = ((ItemKeyValue *)temp[i]).nodeValue;
		}
        
		++j;
		if (j == ELEMENTI)	{
			if ([arrayDati count] < x) {
				[arrayDati addObject:appRecord];
			} 
			
			j = 0;
			
		}
	}
	
	NSLog(@"%d",[arrayDati count]);
	
	[self.table setHidden:NO];
	[self.loading stopAnimating];
	[self.table reloadData];
	
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [arrayDati count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellServizio";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    int nodeCount = [self.arrayDati count];
    
    if (cell == nil) {
		[[NSBundle mainBundle] loadNibNamed:@"ServizioCell" owner:self options:nil];
		cell = miaCell;
		self.miaCell = nil;
    }
	
	/*NSMutableArray *item = [arrayDati objectAtIndex:indexPath.row];
     UILabel *titolo = (UILabel *)[cell viewWithTag:1];
     [titolo setText: [item objectAtIndex:1]];
     NSLog(@"%@",[item objectAtIndex:2]);*/
    
    if (nodeCount > 0)
	{
        // Set up the cell...
        AppRecord *appRecord = (self.arrayDati)[indexPath.row];
        
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		
		[dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm:ss"];
		NSDate *pubDate = [dateFormatter dateFromString: appRecord.pubDate];
		
		
		NSLocale *itLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"];
		[dateFormatter setLocale:itLocale];
		[dateFormatter setDateStyle:NSDateFormatterLongStyle];
		[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
		
		
		UILabel *label = (UILabel *)[cell viewWithTag:1];
		[label setText: [NSString stringWithFormat: @"%@",[dateFormatter stringFromDate:pubDate]]];
        [label setTextColor:[UIColor darkGrayColor]];
		
		label = (UILabel *)[cell viewWithTag:2];
		[label setText: appRecord.title];
        [label setTextColor:[UIColor darkGrayColor]];
		
        // Only load cached images; defer new downloads until scrolling ends
        if (!appRecord.appIcon)
        {	
			UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
			if ([appRecord.imageURLString isEqualToString:@""]){ //string is empty
				[imageView setHidden:YES];
				UILabel *label = (UILabel *)[cell viewWithTag:1];
				label.frame = CGRectMake(imageView.frame.origin.x, label.frame.origin.y, label.frame.size.width + imageView.frame.size.width, label.frame.size.height);
				label = (UILabel *)[cell viewWithTag:2];
				label.frame = CGRectMake(imageView.frame.origin.x, label.frame.origin.y, label.frame.size.width + imageView.frame.size.width, label.frame.size.height);
			} else {
				[imageView setHidden:NO];
				if (self.table.dragging == NO && self.table.decelerating == NO)
				{
					[self startIconDownload:appRecord forIndexPath:indexPath];
				}
				imageView.image = [UIImage imageNamed:@"camera"];
			}
        }
        else
        {
			UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
            imageView.image = appRecord.appIcon;            
        }
		
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

#define kLoadingTag 729

- (void)moviePlayerPlaybackStateDidChange:(id)anObject{
    for (UIView* aView in [_moviePlayerView.view subviews]){
        if (aView.tag == kLoadingTag && [aView isKindOfClass:[UIActivityIndicatorView class]]){
            [(UIActivityIndicatorView*)aView stopAnimating];
            break;
        }
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:_moviePlayerView.moviePlayer];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   /* if (![[TeleTicinoAppDelegate shared] inWifi]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:kNotInWifiMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [alert release];
        return;
    }*/
    [table deselectRowAtIndexPath:indexPath animated:YES];
    NSString *sVideoUrl = [[NSString alloc]initWithString:((AppRecord *)arrayDati[indexPath.row]).detailURLString];
    NSLog(@"%@",sVideoUrl);
    
    NSURL *videoURL = [NSURL URLWithString:sVideoUrl];;
    self.moviePlayerView = [[DOSMoviePlayerController alloc] initWithContentURL:videoURL];
	[_moviePlayerView.view setBackgroundColor:[UIColor blackColor]];
	UIImageView *backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"radio3i-bg"]];
	[backImage setFrame:_moviePlayerView.view.frame];
	[backImage setContentMode:UIViewContentModeScaleAspectFit];
    [backImage setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackStateDidChange:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:_moviePlayerView.moviePlayer];
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        [_moviePlayerView setOrientationMask:UIInterfaceOrientationMaskPortrait];
    } else {
        [_moviePlayerView setOrientationMask:UIInterfaceOrientationMaskLandscape];
    }
    
	[_moviePlayerView.moviePlayer.backgroundView addSubview:backImage];
    [[StreamingViews sharedInstance] closeAllPlayingViewBut:nil];
//    moviePlayerView.moviePlayer.useApplicationAudioSession = YES;
	[_moviePlayerView.view sendSubviewToBack:backImage];
	[self presentViewController:_moviePlayerView animated:YES completion:nil];
}

#pragma mark -
#pragma mark Table cell image support

- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath{
    IconDownloader *iconDownloader = imageDownloadsInProgress[indexPath];
	
    if (iconDownloader == nil) {
        iconDownloader = [[IconDownloader alloc] init];
		[iconDownloader setResize:YES];
        iconDownloader.appRecord = appRecord;
        iconDownloader.indexPathInTableView = indexPath;
        iconDownloader.delegate = self;
        imageDownloadsInProgress[indexPath] = iconDownloader;
        [iconDownloader proportionalImageWithWidth:74];
        [iconDownloader startDownload];
	}
}

// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
- (void)loadImagesForOnscreenRows{
    if ([self.arrayDati count] > 0){
        NSArray *visiblePaths = [self.table indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths){
            AppRecord *appRecord = (self.arrayDati)[indexPath.row];
            
            if (!appRecord.appIcon) // avoid the app icon download if the app already has an icon
			{
                [self startIconDownload:appRecord forIndexPath:indexPath];
            }
        }
    }
}


// called by our ImageDownloader when an icon is ready to be displayed
- (void)appImageDidLoad:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = imageDownloadsInProgress[indexPath];
    if (iconDownloader != nil)
    {
        UITableViewCell *cell = [self.table cellForRowAtIndexPath:iconDownloader.indexPathInTableView];
        
        // Display the newly loaded image
		UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
		imageView.image =iconDownloader.appRecord.appIcon;
        
    }
}

#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)

// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self loadImagesForOnscreenRows];
}

@end

