//
//  Radio3i_UIViewController.h
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CoreAnimation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Contatta_UIViewController.h"
#import "StreamingViewControllerCommon.h"
#import "Playlist_UIViewController.h"
#import "Podcast_UIViewController.h"
#import "StreamingViews.h"

@interface Radio3i_UIViewController : StreamingViewControllerCommon <UIWebViewDelegate, Podcast_UIViewControllerDelegate> {
    
}

@property (nonatomic, strong) NSString *radioState;
@property (nonatomic, strong) NSString *tgState;

@property (nonatomic, strong) NSTimer *progressUpdateTimer;
@property (nonatomic, strong) UIPopoverController *popoverCont;
@property (nonatomic, unsafe_unretained) BOOL seeking;
@property (nonatomic, unsafe_unretained) BOOL timerIsCountingDown;
@property (nonatomic, unsafe_unretained) double newSeekTime;
@property (nonatomic, unsafe_unretained) double progress;
@property (nonatomic, unsafe_unretained) double duration;
@property (nonatomic, strong) Contatta_UIViewController * contatta;
@property (nonatomic, strong) Playlist_UIViewController * playlist;
@property (nonatomic, strong) Podcast_UIViewController * podcast;
@property (nonatomic, weak) IBOutlet UISlider *progressSlider;
@property (nonatomic, weak) IBOutlet UILabel *playedLabel;
@property (nonatomic, weak) IBOutlet UILabel *remainingLabel;
@property (nonatomic,strong) MPVolumeView *volumeView;
@property (nonatomic, unsafe_unretained) BOOL waitingForStop;

@property (nonatomic,weak) IBOutlet UIButton *buttonRadio;
@property (nonatomic,weak) IBOutlet UIButton *buttonTg;

@property (nonatomic, weak) IBOutlet UIView *volumeSlider;
@property (nonatomic, weak) IBOutlet UIView *progressView;

- (IBAction) podcast:(id)sender;
- (IBAction) contatta:(id)sender;
- (IBAction) playlist:(id)sender;
- (IBAction)sliderTouchUpInside:(UISlider *)aSlider;
- (IBAction)sliderMoved:(UISlider *)aSlider;

- (UIButton *)activeButton;

- (void) spinButton:(UIButton *)button;
- (void) createStreamer:(NSString *)url;
- (void) showProgressSlider;
- (void) hideProgressSlider;
- (void) setButtonImage:(UIImage *)image forState:(int)controlState;
- (void) updateProgress:(NSTimer *)aNotification;
- (void) setStopped;
- (IBAction)goToStore:(id)sender;

@end
