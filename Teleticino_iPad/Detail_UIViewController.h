//
//  Detail_UIViewController.h
//  TeleTicino
//
//  Created by Antonio Egizio on 15/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataAccess.h"
#import "AppRecord.h"
#import "IconDownloader.h"
#import "Common.h"
#import "NSData+Base64.h"
#import "GAITrackedViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@protocol DetailViewDelegate;

@interface Detail_UIViewController : GAITrackedViewController <IconDownloaderDelegate, UIWebViewDelegate> {
    
}

@property (weak, nonatomic) IBOutlet UIView *bannerContainer;
@property (nonatomic, unsafe_unretained) BOOL webViewFinishLoading;
@property (nonatomic, unsafe_unretained) BOOL imageFinishedDownload;
@property (nonatomic, unsafe_unretained) BOOL firstLoad;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, strong) NSMutableArray *arrayDati;
@property (nonatomic, strong) IconDownloader *iconDownloader;
@property (nonatomic, unsafe_unretained) BOOL alreadyRotated;
@property (nonatomic, strong) DataAccess *dataAccess;
@property (nonatomic, unsafe_unretained) int orientation;
@property (nonatomic, unsafe_unretained) int articleNumber;
@property (nonatomic, strong) NSString *section;
@property (nonatomic, strong) NSString *factory;
@property (nonatomic, strong) NSString *myTitle;
@property (nonatomic, strong) NSString *mySubtitle;
@property (nonatomic, unsafe_unretained) id <DetailViewDelegate> delegate;
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *html;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) NSTimer *bannerTimer;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerTrailingConstraint;


- (IBAction)dismissAction:(id)sender;
-(void) populateView:(NSMutableData *)data;
@end

@protocol DetailViewDelegate

- (void) presentDetail:(Detail_UIViewController *)detail;
- (void) dismissAndRepresent;

@end
