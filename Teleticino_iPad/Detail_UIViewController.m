//  Detail_UIViewController.m
//  TeleTicino
//
//  Created by Antonio Egizio on 15/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "Detail_UIViewController.h"
#import "NSString+HTML.h"
#import "Teleticino_iPadAppDelegate.h"
#import "AGWindowView.h"


#define kHTMLBody @"<!DOCTYPE HTML><html><head><style>body {padding:10px;} </style><script type=\"text/javascript\">function resizeText(multiplier) {if (document.body.style.fontSize == \"\") {document.body.style.fontSize = \"1.0em\";}document.body.style.fontSize = parseFloat(document.body.style.fontSize) + (multiplier * 0.2) + \"em\";}</script><meta name='viewport' content='width=device-width; initial-scale=1.0; maximum-scale=1.0;'><style type=\"text/css\">.pubDate{font-family: Replica-Regular;color:#4a4a4a}.image {margin-right:10px; }.title{font-weight:bold;font-size:2em; margin-bottom:15px;text-align:left;color:#e00a25;font-family: Replica-Bold;}.subtitle{font-family: Replica-Regular;font-size:1.5em;}.text{ float:right; width:80%; }.cont{font-size:1em;font-family: Replica-Regular;}</style></head><body>"


@interface Detail_UIViewController (Private)


- (void)startIconDownload:(AppRecord *)appRecord;
- (void)loadContent;

@end

@interface Detail_UIViewController() <GADBannerViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *topBannerWrapperView;
@property (weak, nonatomic) IBOutlet UIImageView *closeButtonImageView;
@property (strong, nonatomic) DFPRequest *request;
@property (strong, nonatomic) DFPBannerView *topBannerView;
@property (nonatomic, strong) NSString *myUrl;
@property (weak, nonatomic) IBOutlet DFPBannerView *bannerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBannerWrapperConstrainWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBannerWrapperConstrainHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomBanerHeight;

@end


@implementation Detail_UIViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissAction:)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRed:224.0/255.0 green:10.0/255.0 blue:37.0/255.0 alpha:1.0];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ticinonews_logo"]];
    
    if ([self respondsToSelector:@selector(loadBanner)]) {
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadBanner) userInfo:nil repeats:NO];
    }
    
    StatSendOperation *stats = [[StatSendOperation alloc] init];
    [stats sendStats];
    
    self.arrayDati = [[NSMutableArray alloc] init];
    self.dataAccess = [[DataAccess alloc] init];
    self.alreadyRotated = NO;
    
    [_webView setHidden:YES];
    [_loading startAnimating];
    self.webViewFinishLoading = NO;
    self.imageFinishedDownload = NO;
    self.firstLoad = YES;
    //webView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [_dataAccess SetItemsReceivedSelector:self :@selector(populateView:)];
    NSLog(@"%@ %d", _section, _articleNumber);
    //	if ([section isEqualToString:@"TopnewsDetail"])
    //		[dataAccess GetTeleTicinoData:section];
    //	else
    [_dataAccess GetDetailedData:_factory withSection:_section articleNumber:_articleNumber];
    
    //Nascondiamo l'ombra della webView
    for(UIView *wview in [[_webView subviews][0] subviews]) {
        if([wview isKindOfClass:[UIImageView class]]) { wview.hidden = YES; }
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = NSStringFromClass([self class]);
}


- (IBAction)share:(id)sender {
    NSString *textToShare = _myTitle;
    NSURL *myWebsite = [NSURL URLWithString:_myUrl];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
    [popup presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - Admob

-(DFPRequest *)request {
    if (_request == nil) {
        _request = [DFPRequest request];
    }
    return _request;
}

-(void)loadBanner {
    self.topBannerView = [[DFPBannerView alloc]initWithFrame:CGRectZero];
     self.topBannerView.validAdSizes = @[NSValueFromGADAdSize(kGADAdSizeMediumRectangle), NSValueFromGADAdSize(GADAdSizeFromCGSize(CGSizeMake(600, 500)))];
    
    for (GADBannerView *bannerView in @[self.bannerView, self.topBannerView]) {
        bannerView.adUnitID = AD_ID;
        bannerView.rootViewController = self;
        bannerView.delegate = self;
        [bannerView loadRequest:self.request];
    }
    
    if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft
        ||[[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight) {
        self.bannerView.adSize = kGADAdSizeSmartBannerLandscape;
    }
    else {
        self.bannerView.adSize = kGADAdSizeSmartBannerPortrait;
    }
}

#pragma mark - Gadbanner delegate

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    if (bannerView == self.topBannerView) {
        self.topBannerWrapperConstrainWidth.constant = bannerView.frame.size.width;
        self.topBannerWrapperConstrainHeight.constant = bannerView.frame.size.height;
        [self.topBannerWrapperView addSubview:self.topBannerView];
        self.closeButtonImageView.hidden = NO;
        [self.topBannerWrapperView addSubview:self.closeButtonImageView];
        [self.topBannerWrapperView setHidden:NO];
    }
    else if (bannerView == self.bannerView) {
        self.bottomBanerHeight.constant = 90;
    }
}

-(void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed received ad with error: %@", [error localizedDescription]);
    if (bannerView == self.topBannerView) {
        [self.topBannerWrapperView setHidden:YES];
    }
    else if (bannerView == self.bannerView) {
        self.bottomBanerHeight.constant = 0;
    }
}

#pragma mark - Gesture recognizer
- (IBAction)didTapCloseButton:(UITapGestureRecognizer *)sender {
    self.topBannerWrapperView.hidden = YES;
}

//#define ELEMENTI 7

- (void) traverseElement:(TBXMLElement *)element {
    
    do {
        // Display the name of the element
        
        // Obtain first attribute from element
        //TBXMLAttribute * attribute = element->firstAttribute;
        
        NSString *elementName = [TBXML elementName:element];
        /*
         if ([elementName length]) {
         AppRecord *appRecord = [[AppRecord alloc] init];
         NSString *appID = [TBXML valueOfAttributeNamed:@"id" forElement:element];
         NSString *title = [TBXML valueOfAttributeNamed:@"title" forElement:element];
         NSString *imageURLString = [TBXML valueOfAttributeNamed:@"imageURLString" forElement:element];
         NSString *detailURLString = [TBXML valueOfAttributeNamed:@"detailURLString" forElement:element];
         
         NSLog(appID);
         NSLog(title);
         NSLog(imageURLString);
         NSLog(detailURLString);
         
         appRecord.appID = appID;
         appRecord.title = title;
         appRecord.imageURLString = imageURLString;
         appRecord.detailURLString = detailURLString;
         
         [arrayDati addObject:appRecord];
         [appRecord release];
         }
         */
        
        if ([elementName isEqualToString:@"news"]) {
            AppRecord *appRecord = [[AppRecord alloc] init];
            
            TBXMLElement *appID = [TBXML childElementNamed:@"id" parentElement:element];
            TBXMLElement *pubDate = [TBXML childElementNamed:@"pubDate" parentElement:element];
            TBXMLElement *title = [TBXML childElementNamed:@"title" parentElement:element];
            TBXMLElement *subtitle = [TBXML childElementNamed:@"subtitle" parentElement:element];
            TBXMLElement *imageURLString = [TBXML childElementNamed:@"img" parentElement:element];
            TBXMLElement *detailURLString = [TBXML childElementNamed:@"link" parentElement:element];
            
            if (appID) appRecord.appID = [TBXML textForElement:appID];
            if (title) appRecord.title = [TBXML textForElement:title];
            if (subtitle) appRecord.subtitle = [TBXML textForElement:subtitle];
            if (pubDate) appRecord.pubDate = [TBXML textForElement:pubDate];
            if (imageURLString) appRecord.imageURLString = [TBXML textForElement:imageURLString];
            if (detailURLString) {
                appRecord.detailURLString = [TBXML textForElement:detailURLString];
                self.myUrl = appRecord.detailURLString;
            } else {
                self.myUrl = @"";
            }
            
            if ([appRecord.imageURLString length] > 0 ) {//string not empty
                NSString *textToCut = @"_rettverticale";
                NSRange cutLocation = [appRecord.imageURLString rangeOfString:textToCut];
                if (cutLocation.location > 0){
                    NSString *newImageUrl = [[NSString alloc] initWithFormat:@"%@_header.jpg",[appRecord.imageURLString substringToIndex:cutLocation.location]];
                    //[appRecord.imageURLString release];
                    appRecord.imageURLString = [newImageUrl copy];
                }
            }
            
            [_arrayDati addObject:appRecord];
        }
        
        // if the element has child elements, process them
        //if (element->firstChild) [self traverseElement:element->firstChild];
        
        // Obtain next sibling element
    } while ((element = element->nextSibling));
    
}

- (IBAction)dismissAction:(id)sender {
    [_bannerTimer invalidate];
    self.bannerTimer = nil;
    [self dismissModalViewControllerAnimated:YES];
}

-(void)populateView:(NSMutableData *)data {
    
    if (!data){
        [_webView setHidden:YES];
        [_loading setHidden:YES];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Impossibile contattare il server, connettività limitata o assente. Verifica la tua connessione a Internet e riprova. Se il problema persiste riprova più tardi." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        return;
    }
    
    TBXML * tbxml = [TBXML tbxmlWithXMLData:data];
    
    if (tbxml.rootXMLElement && (tbxml.rootXMLElement->firstChild))
    {
        [self traverseElement:tbxml.rootXMLElement->firstChild];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Errore" message:@"Errore di rete" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
    NSLog(@"%lu",(unsigned long)[_arrayDati count]);
    [self loadContent];
}

- (void)loadContent{
    self.firstLoad = YES;
    if ([_arrayDati count]) [self startIconDownload:_arrayDati[0]];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSURL* url = [request URL];
    if (UIWebViewNavigationTypeLinkClicked == navigationType){
        [[UIApplication sharedApplication] openURL:url];
        return NO;
    }
    if([[[request URL] absoluteString] rangeOfString:@"mailto:"].location != NSNotFound)
        return YES;
    return _firstLoad;
}

- (void)startIconDownload:(AppRecord *)appRecord{
    if (_iconDownloader == nil){
        self.iconDownloader = [[IconDownloader alloc] init];
        _iconDownloader.appRecord = appRecord;
        _iconDownloader.indexPathInTableView = nil;
        _iconDownloader.delegate = self;
        [_iconDownloader proportionalImageWithWidth:300];
        [_iconDownloader startDownload];
    }
}

- (void)appImageDidLoad:(NSIndexPath *)indexPath{
    if (_iconDownloader != nil){
        
        [_webView setOpaque:YES];
        [_webView setBackgroundColor:[UIColor clearColor]];
        
        NSData *downloadedImage = UIImageJPEGRepresentation(_iconDownloader.appRecord.appIcon, 1.0);
        NSString *src = [[NSString alloc] initWithFormat:@"data:image/jpeg;base64,%@",[downloadedImage base64EncodedString]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm:ss"];
        NSDate *pubDate = [dateFormatter dateFromString:_iconDownloader.appRecord.pubDate];
        NSLocale *itLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"];
        [dateFormatter setLocale:itLocale];
        [dateFormatter setDateStyle:NSDateFormatterLongStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        /*NSMutableString * temp = [[NSMutableString alloc] initWithString:[iconDownloader.appRecord.title stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"]];
         [temp replaceOccurrencesOfString:@"&nbsp;" withString:@" " options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp length])];
         [temp replaceOccurrencesOfString:@"&gt;" withString:@">" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp length])];
         NSLog(@"%@",temp);
         [temp release];*/
        NSString *tmp = [[NSString alloc] initWithString:[_iconDownloader.appRecord.title stringByConvertingHTMLToPlainText]];
        
        //NSString *html;
        
        NSString *plusImage = @"iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/mAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAlRJREFUeNrsmL1PIkEUwB/rKuFAsjGG6OVIsKahlI6SEguxxAvXE/6Cwz/Ar1qNUAokUhIa7IwVNNQSibnGeAtkJQaCFhNfnsvusHxsoJgXitlZYH8z7zdvBhxP8BOWLyRYyhBYAktgCSyBJbAAtmoV/8cze7li0aXAckbCq6EgXq6n/iwFljsR11HKAf+CsSTF6z6M6zp/JPYXjOU+PDDqjC8Yy5NKYvv97p415IDfVvGlsbKjRr1S+X/6LxFuf2FYVHYtV+jXG/16g126YtGJxJcDfmck7IyEJcU7ExaVfdBs9UplAOieX04n/sb1qa9a9FWLHgv1RbIou5bNYyqHasdu8blYZDLecgXWGKodNm22im+KtRoKYmXvlcqDZgtv0TzaJL4pFt1htK+pYjGL+DNhSYoXs4Oy06CgdlR82bDXFYviMkbZv2Flb5TTDIrfOTrR1YKVwC/dR7BnLRR0RsK6u/16A1cSADgMf+xv1Soo1r+dXSoWXfC4El/2knRGfdXi6IP50c4c07HJfNkBYPP2io6Dvo2KP5roOSdRd5yijzcLJj5O6lBtT8rx0e7SS30SJcW7/fhgZX/QhZrOdM8u+OdbNsLu2YWazkw2W1T2QbNl6DtVG/XypJJ8LDTBUIkxWDSDL3tJrE/8DRgbePKZZ92istOayQktlzc7W88Ni1PZTbGyebpzTyHleCy6jWjZG4tfQd9pZdlOv/nQObCQxwLMO+Tvi6U9aszY6Ncb73f3THzOYF5/p9n+Y0VZh/inWWAJLIElsASWwAIAgM8BAKn27WGcXL34AAAAAElFTkSuQmCC";
        
        NSString *minusImage = @"iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR/mAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAa9JREFUeNrs2D9PwkAUAPAHtJJarY2DIYYm3evQlY1NNssAjpDgzuAXgA+AxN1Eu4IDI2ERJ0ZZ2BuBjQEwQAj/HJ65ECTEkko7vDc07TXt/XL37l1T3ydcgvfCD54MYhGLWMQiFrGIRSxiAacqyqqrrLrnz0UPsY5TCTwRjJhflrzCEtPJn1fIkmDEPMESjBinKgAwrTcA4DR75wmWmEqgaZAvAACva6h0k8WpCs7ayCxN64251QaAk2zGZRaalv3h6KUEAHgU07cus3BgJpUqXo7NMiY+WwQusILRCKbR1+MTtsytNhLFlGMszn6yJwFg1mzNmi3WODLLghFDMaYapyqssLEYm2W86zCLzVRAVS7eXtfbWY0d5h8AIKCGz3L3G49P3xv/wmJ57ZelYDSytcYia2F1BrnCxt2F1fljRz5bv0ZCHzVe1yaVKkssFkf6lVzMAUAvnmGrYe+wweJ1LfRR29ExQ/fimcOtROHmen3d/Q4cQkf2R9sFAovn1phUqsv+0JECYWMSOVUJqOFZs7Wjb17X/LKE+/eBWPTRTCxiEYtYxCIWsYi1R3wPAAPWlBq/0CWbAAAAAElFTkSuQmCC";
        
        if (![_iconDownloader.appRecord.imageURLString length])
        {
            self.html = [[NSString alloc] initWithFormat:@"%@<img src=\"data:image/gif;base64,%@\" align=\"right\" onclick=\"resizeText(+1)\" width=\"28\" height=\"28\" id=\"header\" class=\"image\" /><img src=\"data:image/gif;base64,%@\" align=\"right\" onclick=\"resizeText(-1)\" width=\"28\" height=\"28\" id=\"header\" class=\"image\" /><div ALIGN=CENTER class=\"title\">%@</div><br /><span class=\"pubDate\">%@</span><br /><span class=\"subtitle\">%@</span><br />%@</body></html>", kHTMLBody, plusImage, minusImage, _myTitle ? _myTitle:@"",[dateFormatter stringFromDate:pubDate], _mySubtitle ? _mySubtitle:@"",[tmp stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"]];
        }
        
        else
        {
            self.html = [[NSString alloc] initWithFormat:@"%@<img src=\"%@\" align=\"left\" width=\"%d\" height=\"%d\" id=\"header\" class=\"image\" /><img src=\"data:image/gif;base64,%@\" align=\"right\" onclick=\"resizeText(+1)\" width=\"28\" height=\"28\" id=\"header\" class=\"image\" /><img src=\"data:image/gif;base64,%@\" align=\"right\" onclick=\"resizeText(-1)\" width=\"28\" height=\"28\" id=\"header\" class=\"image\" /><div ALIGN=CENTER class=\"title\">%@</div><span class=\"pubDate\">%@</span><br /><span class=\"subtitle\">%@</span><br /><span class=\"cont\">%@</span></body></html>", kHTMLBody, src, _iconDownloader.imageWidth, _iconDownloader.imageHeight, plusImage, minusImage, _myTitle ? _myTitle:@"", [dateFormatter stringFromDate:pubDate],  _mySubtitle ? _mySubtitle:@"",tmp];
        }
        
        [_webView loadHTMLString:self.html baseURL:nil];
        
        self.imageFinishedDownload = YES;
        self.iconDownloader = nil;
    }
    if (_webViewFinishLoading){
        [_loading stopAnimating];
        [_webView setHidden:NO];
    }
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    self.webViewFinishLoading = YES;
    if (_imageFinishedDownload) {
        [self appImageDidLoad:nil];
        self.firstLoad = NO;
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [_closeButton setHidden:NO];
    
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        self.bannerView.adSize = kGADAdSizeSmartBannerLandscape;
        [self.bannerView loadRequest:self.request];
    }
    else {
        self.bannerView.adSize = kGADAdSizeSmartBannerPortrait;
    }
    
    [_closeButton setHidden:YES];
}

- (void)dealloc {
    
    [_dataAccess SetItemsReceivedSelector:nil :@selector(populateView:)];
    if (_iconDownloader){
        [_iconDownloader cancelDownload];
    }
    _webView.delegate = nil;
    [_webView stopLoading];
    [_arrayDati removeAllObjects];
}


@end
