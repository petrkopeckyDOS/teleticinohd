//
//  Constants.m
//  ticinonews
//
//  Created by Petr Kopecky on 30.03.17.
//  Copyright © 2017 Dos Informatica ed Elettronica Sagl. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const VIDEO_AD_TAG;
extern NSString *const AD_ID;
