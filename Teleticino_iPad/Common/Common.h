//
//  Common.h
//  cerca e trova
//
//  Created by Magnus Pacheco on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "TBXML.h"


@interface Common : NSObject {
	
}

#pragma mark View
+ (void)ShowView:(UIView *)parentView ViewToShow:(UIView *)viewToShow;
+ (void)ShowView:(UIView *)parentView ViewToShow:(UIView *)viewToShow TransitionType:(NSString *)type TransitionSubType:(NSString *)subType;
#pragma mark -

#pragma mark XML
+ (NSMutableArray *) XMLGetItemsBelowNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName;
+ (NSMutableArray *) XMLGetItemsValuesByNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName;
+ (NSMutableArray *) XMLGetItemsValuesBelowNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName;
+ (void*) XMLGetItemsBelowNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName ItemsArray:(NSMutableArray *)itemsArray;
+ (void*) XMLGetItemsValuesByNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName ItemsArray:(NSMutableArray *)itemsArray;
+ (void*) XMLGetItemsValuesBelowNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName ItemsArray:(NSMutableArray *)itemsArray;
+ (void*) XMLGetAllItemsBelowNode:(TBXMLElement *)element ItemsArray:(NSMutableArray *)itemsArray;
+ (void*) XMLGetAllItemsValuesBelowNode:(TBXMLElement *)element ItemsArray:(NSMutableArray *)itemsArray;

#pragma mark -


@end

@interface ItemKeyValue : NSObject {
	
	NSString *nodeKey;
	NSString *nodeValue;
}

@property (nonatomic, retain) NSString *nodeKey;
@property (nonatomic, retain) NSString *nodeValue;

@end