//
//  DataAccess.m
//  cerca e trova
//
//  Created by Magnus Pacheco on 02.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DataAccess.h"


@implementation Config


- (id) init {
    
    self = [super init];
    if (self) {
        hostnameFiltroItems = @"http://example.com/iPhone/FiltroItems.jsp?";
    }
		
	return self;
}

@end


@implementation DataAccess

- (id) init {
	
    self = [super init];
    if (self) {
        
    }

	return self;
}

- (void) GetTeleTicinoData:(NSString *)sorgente {
	NSString *urlString = @"";
    if ([sorgente isEqualToString:@"Podcast"])
		urlString = @"http://www.ticinonews.ch/ext/xml/radio3i/servizi-programmi.php";
	else if ([sorgente isEqualToString:@"telegiornali"])
		urlString = @"http://www.ticinonews.ch/ext/xml/teleticino/elenco-tg.php";
	else if ([sorgente isEqualToString:@"servizi"])
		urlString = @"http://www.ticinonews.ch/ext/xml/teleticino/servizi-tg-tgs.php";
	else if ([sorgente isEqualToString:@"trasmissioni"])
		urlString = @"http://www.ticinonews.ch/ext/xml/teleticino/programmi.xml";
	else if ([sorgente isEqualToString:@"palinsesto"])
		urlString = @"http://www.ticinonews.ch/ext/xml/teleticino/palinsesto.php";
	else if ([sorgente isEqualToString:kTopnews])
		urlString = @"http://www.ticinonews.ch/ext/xml/ticinonews/newslist.php?s=topnews";
	else if ([sorgente isEqualToString:@"TopnewsDetail"])
		urlString = @"http://www.ticinonews.ch/ext/xml/ticinonews/apertura_detail.php";
	else if ([sorgente isEqualToString:kTicino])
		urlString = @"http://www.ticinonews.ch/ext/xml/ticinonews/newslist.php?s=ticino";
	else if ([sorgente isEqualToString:kSvizzera])
		urlString = @"http://www.ticinonews.ch/ext/xml/ticinonews/newslist.php?s=svizzera";
	else if ([sorgente isEqualToString:kEstero])
		urlString = @"http://www.ticinonews.ch/ext/xml/ticinonews/newslist.php?s=estero";
	else if ([sorgente isEqualToString:kSport])
		urlString = @"http://www.ticinonews.ch/ext/xml/ticinonews/newslist.php?s=sport";
    else if ([sorgente isEqualToString:@"Fuorigioco"])
        urlString = @"http://www.ticinonews.ch/ext/xml/teleticino/servizi-tgs.php";
	else if ([sorgente isEqualToString:kServizitg])
		urlString = @"http://www.ticinonews.ch/ext/xml/teleticino/servizi-tg-tgs.php";
	else if ([sorgente isEqualToString:@"playlist"])
		urlString = @"http://www.ticinonews.ch/ext/xml/radio3i/playlist.php";
	else if ([sorgente isEqualToString:@"ReplayProgrammi"])
		urlString = @"http://www.ticinonews.ch/ext/xml/teleticino/servizi-programmi.php";
	else {
		urlString = [NSString stringWithFormat:@"http://www.ticinonews.ch/xml/teleticino/programmi/%@.aspx",sorgente];
		NSLog(@"%@",urlString);
	}
	
	[self UrlDownload:urlString];
}

- (void) GetDetailedData:(NSString*)factory withSection:(NSString *)section articleNumber:(int)aNumber {
    NSString *urlString = [NSString stringWithFormat:@"http://ticinonews.ch/xml/ticinonews/ticinonewsdetailsid.aspx?id=%d", aNumber];
    
	NSLog(@"%@",urlString);
	[self UrlDownload:urlString];
}


#pragma mark -
#pragma mark Download

- (IBAction) GetFilterItems:(NSString *)filter {

	NSString *urlString = @"http://www.gdp.ch/iphone/ultime-edizioni.php";
	
	[self UrlDownload:urlString];

}

- (void)DownloadOfferte:(NSString *)macrorubrica rubrica:(NSString *)rubrica sottorubrica:(NSString *)sottorubrica microrubrica:(NSString *)microrubrica nazione:(NSString *)nazione cantone:(NSString *)cantone provincia:(NSString *)provincia area:(NSString *)area localita:(NSString *)localita {

	
}

- (IBAction)DownloadTest {
	NSString *urlString = @"http://www.gdp.ch/iphone/ultime-edizioni.php";
	
	[self UrlDownload:urlString];
	
/*	NSURL *url = [NSURL URLWithString:urlString];
	
	NSURLConnection nsURLConnection = [NSURLConnection  
	
	
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	
	[request setDidFinishSelector:@selector(ElencoOfferteDone:)];
	[request setDidFailSelector:@selector(ElencoOfferteError:)];
	[request setDelegate:self];
	[request startAsynchronous];
	*/
}


- (void)UrlDownload:(NSString *)url {
	// Create the request.
	NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:url]
				 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
				 timeoutInterval:30.0];
	
	// create the connection with the request
	// and start loading the data
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		
		_receivedData = [NSMutableData data];
	} else {
		   // Inform the user that the connection failed.
		   }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [_receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    // receivedData is declared as a method instance elsewhere
	
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    [_caller performSelector:ItemsReceivedSelector withObject:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	//[self ElencoOfferteDone:receivedData];

	[self ItemsReceived:_receivedData];
		
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    NSLog(@"Succeeded! Received %d bytes of data",[_receivedData length]);
	
    // release the connection, and the data object
}

-(IBAction) SetItemsReceivedSelector:(id)theCaller :(SEL)theSelector {

	_caller = theCaller;
	ItemsReceivedSelector = theSelector;
}

-(IBAction) UnSetItemsReceivedSelector {

	_caller = nil;
	ItemsReceivedSelector = nil;
	
}

- (void)ItemsReceived:(NSMutableData *)rcvData {
	
    [_caller performSelector:ItemsReceivedSelector withObject:rcvData];
	
}

- (void)ElencoOfferteDone:(NSMutableData *)data {
	
	TBXML *tbxml = [TBXML tbxmlWithXMLData:data];
	if (tbxml.rootXMLElement)
		[self ParseOfferteXML:tbxml.rootXMLElement];
	//release resources
//	[loading stopAnimating];
//	[self popolaGiornaliScrollView];
	
}

- (void)ParseOfferteXML:(TBXMLElement *)element {
	
	do {
			NSString *row = [TBXML elementName:element];
			//NSString *valore = [TBXML textForElement:element];
		
			if ([row isEqualToString: @"data"])
				 {
					 //int a = 0;
				 } 
			else
				 {
					 //int i = 0;
				 }
		
		if (element->firstChild) {
			[self ParseOfferteXML:element->firstChild];
		}
		
		// Obtain next sibling element
	} while ((element = element->nextSibling));
}

 #pragma mark -

@end
