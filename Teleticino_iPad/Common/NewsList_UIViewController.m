//
//  NewsList_UIViewController.m
//  TeleTicino
//
//  Created by Antonio Egizio on 18/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "NewsList_UIViewController.h"

@implementation NewsList_UIViewController

@synthesize table,dataAccess,arrayDati,miaCell,loading,imageDownloadsInProgress,headingTab,selected, buttonHeight, buttonWidth;

-(UIColor*) selectedButtonColor {
	//return [UIColor colorWithRed:60.0/255 green:187.0/255 blue:239.0/255 alpha:1.0];
	//return [UIColor colorWithRed:47.0/255 green:107.0/255 blue:205.0/255 alpha:1.0];
	return [UIColor blackColor];
}

#pragma mark -
#pragma mark View lifecycle

- (id)init
{
    self = [super init];
    if (self) {
        buttonWidth = 90;
        buttonHeight = 49;
    }
    return self;
}

- (id)initWithButtonWidth:(int)aWidth AndHeight:(int)aHeight{
    if((self = [super init])){
        //Set default size or custom size
        buttonWidth = aWidth > 0 ? aWidth : 90 ;
        buttonHeight = aHeight > 0 ? aHeight : 49;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])){
        //Set default size
        buttonWidth =  90;
        buttonHeight = 49;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil buttonWidth:(int)aWidth height:(int)aHeight{
    if((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])){
        //Set default size or custom size
        buttonWidth = aWidth > 0 ? aWidth : 90 ;
        buttonHeight = aHeight > 0 ? aHeight : 49;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view.layer setBorderWidth:1.0f];
    [self.view.layer setBorderColor:[UIColor colorWithRed:190.0/255.0 green:190.0/255.0 blue:190.0/255.0 alpha:1.0].CGColor];
    
	[headingTab setShowsHorizontalScrollIndicator:NO];
	[headingTab setShowsVerticalScrollIndicator:NO];
	[self.table setHidden:YES];
	[self.loading startAnimating];	
	self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
	
	[self clearTable];
	
	headingTab.contentSize = CGSizeMake(buttonWidth * [_headings count] + PAD, buttonHeight);
	for (UIButton* button in _headings){
		[headingTab addSubview:button];
		if (button.tag == selected)
			button.selected = YES;
	}
	
	[self scrollToVisibleTabItem];
	self.tabItemIsVisible = YES;
    
}

-(void) loadTable{
	if (!arrayDati)
		arrayDati = [[NSMutableArray alloc] init];
	if (!dataAccess){
		[self.table setHidden:YES];
		dataAccess = [[DataAccess alloc] init];
		[dataAccess SetItemsReceivedSelector:self :@selector(ricaricaTabella:)];
	}
}

- (void) scrollToVisibleTabItem
{
	CGRect visibleRect = CGRectMake(selected * buttonWidth + PAD / 2, 0, buttonWidth, buttonHeight);
	if ((selected!=0)&&(selected!=([_headings count]-1))){
		if (visibleRect.origin.x > (headingTab.contentOffset.x + headingTab.frame.size.width/2))
			visibleRect.origin.x += buttonWidth / 2;
		else 
			visibleRect.origin.x -= buttonWidth / 2;
	}
	self.tabItemIsVisible = YES;
	[headingTab scrollRectToVisible:visibleRect animated:YES];
}

-(void) ricaricaTabella:(NSMutableData *)data {
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [arrayDati count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor clearColor]];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"CellNews";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	// add a placeholder cell while waiting on table data
    int nodeCount = [self.arrayDati count];
	
    if (cell == nil) {
		[[NSBundle mainBundle] loadNibNamed:@"NewsCell" owner:self options:nil];
		cell = miaCell;
		self.miaCell = nil;
    }
    
	// Leave cells empty if there's no data yet
    if (nodeCount > 0)
	{
        // Set up the cell...
        AppRecord *appRecord = (self.arrayDati)[indexPath.row];
        
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		
		[dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm:ss"];
		NSDate *pubDate = [dateFormatter dateFromString: appRecord.pubDate];
		
		NSLocale *itLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"it_IT"];
		[dateFormatter setLocale:itLocale];
		[dateFormatter setDateStyle:NSDateFormatterLongStyle];
		[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
		
		UILabel *label = (UILabel *)[cell viewWithTag:1];
		[label setText: [NSString stringWithFormat: @"%@",[dateFormatter stringFromDate:pubDate]]];	
		
		label = (UILabel *)[cell viewWithTag:2];
		[label setText: appRecord.title];
		label = (UILabel *)[cell viewWithTag:3];
		[label setText: appRecord.subtitle];
		
        // Only load cached images; defer new downloads until scrolling ends
        if (!appRecord.appIcon)
        {	
			UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
			if ([appRecord.imageURLString isEqualToString:@""]){ //string is empty
				[imageView setHidden:YES];
			} else {
				[imageView setHidden:NO];
				if (self.table.dragging == NO && self.table.decelerating == NO)
				{
					[self startIconDownload:appRecord forIndexPath:indexPath];
				}
				// if a download is deferred or in progress, return a placeholder image
				imageView.image = [UIImage imageNamed:@"Placeholder"];
			}
        }
        else
        {
			UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
            imageView.image = appRecord.appIcon;            
        }
    }
	
	if (!_tabItemIsVisible)
		[self scrollToVisibleTabItem];
	
    return cell;
	
	
}

#pragma mark -
#pragma mark Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (selected!=([_headings count]-1)){
		Detail_UIViewController *detail = [[Detail_UIViewController alloc] initWithNibName:@"Detail_UIViewController" bundle:nil];
		UILabel *label = (UILabel *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:2];
		detail.myTitle = label.text;
		label = (UILabel *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:3];
		detail.mySubtitle = label.text;
		NSString *section = ((UIButton *)_headings[selected]).titleLabel.text;
		detail.section = [NSString stringWithString:section];
		detail.factory = @"ticinonews";
		NSLog(@"%@",section);
		detail.articleNumber = indexPath.row + 1;
		//[self.navigationController pushViewController:detail animated:YES];
        CGRect frame = self.table.frame;
        [detail.view setFrame:frame];
        [self.view addSubview:detail.view];
	}
}

#pragma mark -
#pragma mark Table cell image support

- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = imageDownloadsInProgress[indexPath];
    if (iconDownloader == nil) 
    {
        iconDownloader = [[IconDownloader alloc] init];
        iconDownloader.appRecord = appRecord;
        iconDownloader.indexPathInTableView = indexPath;
        iconDownloader.delegate = self;
        imageDownloadsInProgress[indexPath] = iconDownloader;
		[iconDownloader proportionalImageWithWidth:74];
        [iconDownloader startDownload];
	}
}

-(void) clearTable{
    [arrayDati removeAllObjects];
	arrayDati = nil;
	dataAccess = nil;
	NSArray *allDownloads = [self.imageDownloadsInProgress allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
	[imageDownloadsInProgress removeAllObjects];
}


-(void)rubricaTabButtonTouchUpInside:(id)sender{
    [self.table setHidden:YES];
    [self.loading startAnimating];
    [self.table scrollRectToVisible:CGRectMake(0, 0, 0, 0) animated:YES];
    
    UIButton *selectedButton = _headings[selected];
    
    [selectedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedButton.titleLabel.font = [UIFont boldSystemFontOfSize: 14];
    selectedButton.selected = NO;
    [selectedButton setBackgroundColor:[UIColor clearColor]];
	
    ((UIButton*)sender).selected = YES;
    [((UIButton*)sender) setBackgroundColor:[UIColor whiteColor]];
    [((UIButton*)sender) setTitleColor:[self selectedButtonColor] forState:UIControlStateNormal];
    ((UIButton*)sender).titleLabel.font = [UIFont boldSystemFontOfSize: 14];
    
    selected = ((UIButton*)sender).tag;
}

- (void)addHeading:(NSString*)aHeading{
	if (!_headings) {
		self.headings = [[NSMutableArray alloc] initWithCapacity:0];
    }
    CGRect frame = CGRectMake(buttonWidth * [_headings count] + 1, 0, buttonWidth, buttonHeight);
	UIButton *button = [[UIButton alloc]initWithFrame:frame];
	
    [button setTitle:aHeading forState: UIControlStateNormal];
	button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    button.titleLabel.font = [UIFont boldSystemFontOfSize: 14];
    button.titleLabel.shadowOffset    = CGSizeMake (0.0, 0.0);
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
	button.tag = [_headings count];
	
    if (button.tag == (selected)) {
        [button setTitleColor:[self selectedButtonColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize: 14];
        [button setBackgroundColor:[UIColor whiteColor]];
	} else {
        [button setBackgroundColor:[UIColor clearColor]];
    }
    
	[button addTarget:self action:@selector(rubricaTabButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
	
    [_headings addObject:button];
}

// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
- (void)loadImagesForOnscreenRows{
    if ([self.arrayDati count] > 0){
        NSArray *visiblePaths = [self.table indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths){
            AppRecord *appRecord = (self.arrayDati)[indexPath.row];
            
            if (!appRecord.appIcon) // avoid the app icon download if the app already has an icon
            {
                [self startIconDownload:appRecord forIndexPath:indexPath];
            }
        }
    }
}

// called by our ImageDownloader when an icon is ready to be displayed
- (void)appImageDidLoad:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = imageDownloadsInProgress[indexPath];
    if (iconDownloader != nil)
    {
        UITableViewCell *cell = [self.table cellForRowAtIndexPath:iconDownloader.indexPathInTableView];
        
        // Display the newly loaded image
		UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
		imageView.image =iconDownloader.appRecord.appIcon;
        
    }
}

#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)

// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // terminate all pending download connections
    NSArray *allDownloads = [self.imageDownloadsInProgress allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
}

- (void)dealloc {
	
	[_headings removeAllObjects];
	NSArray *allDownloads = [self.imageDownloadsInProgress allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
	[imageDownloadsInProgress removeAllObjects];
}

@end
