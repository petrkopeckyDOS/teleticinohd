//
//  NewsList_UIViewController.h
//  TeleTicino
//
//  Created by Antonio Egizio on 18/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataAccess.h"
#import "IconDownloader.h"
#import "Detail_UIViewController.h"
#import "Common.h"
#import "TBXML.h"
#import "AppRecord.h"

#define PAD 2


@interface NewsList_UIViewController : UIViewController <IconDownloaderDelegate, UITableViewDelegate> {
	int selected;
    int buttonHeight;
    int buttonWidth;
}

@property (nonatomic, unsafe_unretained) BOOL tabItemIsVisible;
@property (nonatomic, strong) NSMutableArray *headings;
@property (nonatomic,unsafe_unretained) int buttonHeight;
@property (nonatomic,unsafe_unretained) int buttonWidth;
@property (nonatomic,unsafe_unretained) int selected;
@property (nonatomic,weak) IBOutlet UITableView *table;
@property (nonatomic,weak) IBOutlet UIScrollView *headingTab;
@property (nonatomic,strong) DataAccess *dataAccess;
@property (nonatomic,strong) NSMutableArray *arrayDati;
@property (nonatomic,weak) IBOutlet UITableViewCell *miaCell;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;


- (id)initWithButtonWidth:(int)aWidth AndHeight:(int)aHeight;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil buttonWidth:(int)aWidth height:(int)aHeight;
- (void)appImageDidLoad:(NSIndexPath *)indexPath;
- (void)addHeading:(NSString*)aHeading;
- (void)clearTable;
- (void)loadTable;
- (void)scrollToVisibleTabItem;
- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath;
- (void)rubricaTabButtonTouchUpInside:(id)sender;
- (UIColor *)selectedButtonColor;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
