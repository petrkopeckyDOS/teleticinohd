//
//  StatSendOperation.h
//  TeleTicino
//
//  Created by Antonio Egizio on 24/03/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface StatSendOperation : NSOperation {
}

- (void)sendStats;

@end
