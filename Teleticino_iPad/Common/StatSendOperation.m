//
//  StatSendOperation.m
//  TeleTicino
//
//  Created by Antonio Egizio on 24/03/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "StatSendOperation.h"

@implementation StatSendOperation


- (void)sendStats {
    
    NSString *url = @"http://ticinews.wemfbox.ch/cgi-bin/ivw/CP/ticinonews/apps/ticinonews/ios/tablet";
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setValue:@"Mozilla/5.0 (iOS-tablet; U; CPU iPad OS like Mac OS X" forHTTPHeaderField:@"User-Agent"];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    
    /*
     NSHTTPURLResponse   * response;
     NSError             * error;
     NSMutableURLRequest * request;
     request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]
     cachePolicy:NSURLRequestReloadIgnoringCacheData
     timeoutInterval:60];
     
     NSArray * availableCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:kBaseUrl]];
     
     if ([availableCookies count]>0){
     NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:availableCookies];
     NSLog(@"Cookie setted! count: %d", [availableCookies count]);
     [request setAllHTTPHeaderFields:headers];
     }
     
     [(TeleTicinoAppDelegate *)[UIApplication sharedApplication].delegate performSelectorOnMainThread:@selector(pollStarted:) withObject:nil waitUntilDone:YES];
     
     if (![NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error])
     [(TeleTicinoAppDelegate *)[UIApplication sharedApplication].delegate performSelectorOnMainThread:@selector(pollFailed:) withObject:error waitUntilDone:YES];
     else {
     
     [(TeleTicinoAppDelegate *)[UIApplication sharedApplication].delegate performSelectorOnMainThread:@selector(pollExecuted:) withObject:nil waitUntilDone:YES];
     
     NSLog(@"RESPONSE HEADERS: \n%@", [response allHeaderFields]);
     
     NSArray * all = [NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:[NSURL URLWithString:kBaseUrl]];
     NSLog(@"How many Cookies: %d", all.count);
     [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:all forURL:[NSURL URLWithString:kBaseUrl] mainDocumentURL:nil];
     
     for (NSHTTPCookie *cookie in all)
     NSLog(@"Name: %@ : Value: %@, Expires: %@", cookie.name, cookie.value, cookie.expiresDate);
     }
     */
}

@end
