//
//  DataAccess.h
//  cerca e trova
//
//  Created by Magnus Pacheco on 02.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TBXML.h"


@interface Config : NSObject {
	
	NSString *hostnameFiltroItems;
	
}

@end


@interface DataAccess : NSObject {
	SEL ItemsReceivedSelector;
}

@property (nonatomic, strong) Config *config;
@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, weak) id caller;

- (IBAction)DownloadTest;

-(IBAction) SetItemsReceivedSelector:(id)theCaller :(SEL)theSelector;
- (IBAction) UnSetItemsReceivedSelector;
- (IBAction) GetFilterItems:(NSString *)filter;

- (void)ItemsReceived:(NSMutableData *)rcvData;
- (void)ParseOfferteXML:(TBXMLElement *)element;
- (void)UrlDownload:(NSString *)url;
- (void) GetTeleTicinoData:(NSString *)sorgente;
- (void) GetDetailedData:(NSString*)factory withSection:(NSString *)section articleNumber:(int)aNumber;

@end
