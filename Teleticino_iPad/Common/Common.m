//
//  Common.m
//  cerca e trova
//
//  Created by Magnus Pacheco on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Common.h"


@implementation Common

#pragma mark View

+ (void)ShowView:(UIView *)parentView ViewToShow:(UIView *)viewToShow {
	
	[Common ShowView:parentView ViewToShow:viewToShow TransitionType: kCATransitionMoveIn TransitionSubType: kCATransitionFromRight];
	
}

+ (void)ShowView:(UIView *)parentView ViewToShow:(UIView *)viewToShow TransitionSubType: subType {
	
	[Common ShowView:parentView ViewToShow:viewToShow TransitionType: kCATransitionMoveIn TransitionSubType: subType];
	
}

+ (void)ShowView:(UIView *)parentView ViewToShow:(UIView *)viewToShow TransitionType:(NSString *)type TransitionSubType:(NSString *)subType {
	
	// get the the underlying UIWindow, or the view containing the current view
	UIView *theWindow = [parentView superview];
	
	// remove the current view and replace with myView1
	//[currentView removeFromSuperview];
	[theWindow addSubview:viewToShow];
	
	// set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	
	/*
	 // Types
	 NSString * const kCATransitionFade;
	 NSString * const kCATransitionMoveIn;
	 NSString * const kCATransitionPush;
	 NSString * const kCATransitionReveal;
	 */
	
	[animation setType:type];
	
	/*
	 // Subtypes
	 NSString * const kCATransitionFromRight;
	 NSString * const kCATransitionFromLeft;
	 NSString * const kCATransitionFromTop;
	 NSString * const kCATransitionFromBottom;
	 */
	
	[animation setSubtype: subType];
	
	
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[theWindow layer] addAnimation:animation forKey:@"SwitchToView1"];
}

#pragma mark -
#pragma mark XML

+ (NSMutableArray *) XMLGetItemsByLevelForTheFirstNode:(TBXMLElement *)element LevelFrom0:(int)level {
	
	NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
	
	int innerLevel = 0;
	
	while (innerLevel < level && element) {
		++innerLevel;
		element = element->firstChild;
	}
	
	while (element) {
		//NSString *row = [TBXML elementName:element];
		NSString *valore = [TBXML textForElement:element];
		[itemsArray addObject:valore];
		
		// Obtain next sibling element
		element = element->nextSibling;
	}
	
	return itemsArray;
}

+ (NSMutableArray *) XMLGetItemsBelowNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName {
	
	NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
	
	[Common XMLGetItemsBelowNodeName:element NodeName:nodeName ItemsArray:itemsArray];
	
	return itemsArray;
}

+ (NSMutableArray *) XMLGetItemsValuesByNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName {
	
	NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
	
	[Common XMLGetItemsValuesByNodeName:element NodeName:nodeName ItemsArray:itemsArray];
	
	return itemsArray;
}

+ (NSMutableArray *) XMLGetItemsValuesBelowNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName {
	
	NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
	
	[Common XMLGetItemsValuesBelowNodeName:element NodeName:nodeName ItemsArray:itemsArray];
	
	return itemsArray;
}

+ (void*) XMLGetItemsBelowNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName ItemsArray:(NSMutableArray *)itemsArray {
	
	while (element) {
		NSString *row = [TBXML elementName:element];
		//NSString *valore = [TBXML textForElement:element];
		if ([row isEqualToString:nodeName])
		{
			[Common XMLGetAllItemsBelowNode:element ItemsArray:itemsArray];
			
		}
		
		if (element->firstChild)
			[Common XMLGetItemsBelowNodeName:element->firstChild NodeName:nodeName ItemsArray:itemsArray];
		
		// Obtain next sibling element
		element = element->nextSibling;
	}
	
	return itemsArray;
}

+ (void*) XMLGetItemsValuesBelowNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName ItemsArray:(NSMutableArray *)itemsArray {
	
	while (element) {
		NSString *row = [TBXML elementName:element];
		//NSString *valore = [TBXML textForElement:element];
		if ([row isEqualToString:nodeName])
		{
			[Common XMLGetAllItemsValuesBelowNode:element ItemsArray:itemsArray];
			
		}
		
		if (element->firstChild)
			[Common XMLGetItemsValuesBelowNodeName:element->firstChild NodeName:nodeName ItemsArray:itemsArray];
		
		// Obtain next sibling element
		element = element->nextSibling;
	}
	
	return itemsArray;
}

+ (void*) XMLGetItemsValuesByNodeName:(TBXMLElement *)element NodeName:(NSString *)nodeName ItemsArray:(NSMutableArray *)itemsArray {
	
	while (element) {
		NSString *name = [TBXML elementName:element];
		NSString *value = [TBXML textForElement:element];
		if ([name isEqualToString:nodeName])
		{
			[itemsArray addObject:value];
		}
		
		if (element->firstChild)
			[Common XMLGetItemsValuesByNodeName:element->firstChild NodeName:nodeName ItemsArray:itemsArray];
		
		// Obtain next sibling element
		element = element->nextSibling;
	}
	
	return itemsArray;
}

+ (void*) XMLGetAllItemsValuesBelowNode:(TBXMLElement *)element ItemsArray:(NSMutableArray *)itemsArray {
	
	if (element && element->firstChild)
	{
		element = element->firstChild;
		
		while (element) {
			//		NSString *row = [TBXML elementName:element];
			NSString *valore = [TBXML textForElement:element];
			
			[itemsArray addObject:valore];
			
			// Obtain next sibling element
			element = element->nextSibling;
		}
	}
	
	return itemsArray;
}

+ (void*) XMLGetAllItemsBelowNode:(TBXMLElement *)element ItemsArray:(NSMutableArray *)itemsArray {
	
	if (element && element->firstChild)
	{
		element = element->firstChild;
		
		while (element) {
			NSString *nodeName = [TBXML elementName:element];
			NSString *nodeValue = [TBXML textForElement:element];
			ItemKeyValue *itemKeyValue = [[ItemKeyValue alloc] init];
			
			itemKeyValue.nodeKey = nodeName;
			itemKeyValue.nodeValue = nodeValue;
			
			[itemsArray addObject:itemKeyValue];
			
			// Obtain next sibling element
			element = element->nextSibling;
		}
	}
	
	return itemsArray;
}





@end

@implementation ItemKeyValue

@synthesize nodeKey, nodeValue;


@end