//
//  Contatta_UIViewController.m
//  TeleTicino
//
//  Created by Antonio Egizio on 07/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "Contatta_UIViewController.h"
#import <QuartzCore/QuartzCore.h>

#define kEmailRegexPattern	@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"

#define kWarning @"Attenzione!"
#define kNotif	 @"Avviso"

#define kSmsSentNotification			@"SMS Inviato con successo"

#define kErrorNotFilledFiels			@"Tutti i campi devono essere riempiti"
#define kErrorNotEstablishedConnection  @"Impossibile stabilire una connessione"
#define kErrorNotEndedRequest			@"Impossibile terminare la richiesta, controllare la connessione di rete e riprovare"
#define kErrorNotValidMail				@"E' stata inserita una mail non valida"

@implementation Contatta_UIViewController

-(void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	isUp = NO;
	editingField = nil;
    
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWasShown:) name: UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWasHidden:) name: UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    StatSendOperation *stats = [[StatSendOperation alloc] init];
    [stats sendStats];
}

# define kOFFSET_FOR_KEYBOARD 150.0     

- (void)setViewMovedUp:(BOOL)movedUp{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
	

	CGRect rect = _container.frame;
	isUp = movedUp;
	if (movedUp) 
		rect.origin.y -= kOFFSET_FOR_KEYBOARD;
	else {
		rect.origin.y += kOFFSET_FOR_KEYBOARD;
        [_container viewWithTag:20].hidden = YES;
    }
	
	_container.frame = rect;
	[UIView commitAnimations];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
  	self.navigationItem.title = @"Scrivi";
    [self.scrollview setContentSize:CGSizeMake(320, 460)];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}


- (void) viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
    [editingField resignFirstResponder];
    if (isUp)
        [self setViewMovedUp:NO];
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name: UIKeyboardWillShowNotification object:nil];
    [nc removeObserver:self name: UIKeyboardWillHideNotification object:nil];
}


-(IBAction)fieldDidBeginEditing:(id)sender{
	editingField = sender;
    if (sender == _testo)
        [_container viewWithTag:20].hidden = NO;
    else 
        [_container viewWithTag:20].hidden = YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
	[self fieldDidBeginEditing:textView];
	if (!isUp)
		[self setViewMovedUp:YES];
}

- (void)dismissKeyboard:(id)sender{
    [editingField resignFirstResponder];
	editingField = nil;
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
	UIBarButtonItem *dismissKeyBoardButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dismisskeyboard"] style:UIBarButtonItemStyleBordered target:self action:@selector(dismissKeyboard:)] ;
	self.navigationItem.rightBarButtonItem = dismissKeyBoardButton;
	if([_testo isFirstResponder])
		[self setViewMovedUp:YES];
}

- (void)keyboardWasHidden:(NSNotification *)aNotification {
	self.navigationItem.rightBarButtonItem = nil;
	if([_testo isFirstResponder])
		[self setViewMovedUp:NO];
}

-(IBAction)clear:(id)sender{
    if (((UIButton *)sender).tag == 20 )
        _testo.text = @"";
    else{
        if (editingField)
            [editingField resignFirstResponder];
        _nome.text = @"";
        _email.text = @"";
        _testo.text = @"";
    }
}

-(void) sendIt{
    NSURL *url = [[NSURL alloc] initWithString:@"http://www.ticinonews.ch/ext/xml/radio3i/sms_iphone.php"];
    NSMutableURLRequest *smsRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    [smsRequest setHTTPMethod:@"POST"];
    [smsRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSString *requestBody = [[NSString alloc]initWithFormat:@"Nome=%@&Email=%@&Testo=%@",[_nome.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],_email.text,[_testo.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *length = [[NSString alloc] initWithFormat:@"%d",[requestBody length]];
    [smsRequest addValue:length forHTTPHeaderField:@"Content-length"];
    [smsRequest setHTTPBody:[requestBody dataUsingEncoding:NSASCIIStringEncoding]];
    [smsRequest setTimeoutInterval:30];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:smsRequest delegate:self];
    
    if (connection) {
        receivedData = [NSMutableData data];
    } else {
        [[[UIAlertView alloc] initWithTitle:kWarning message:kErrorNotEstablishedConnection delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
    
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0){
        [_testo becomeFirstResponder];
    }
    else{
        [self sendIt];
    }
}

-(IBAction)inviaButtonTouchUpInside:(id)sender{
	NSPredicate *predicate;
    NSString *emailRegex = kEmailRegexPattern;
	predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	BOOL emailResult = [predicate evaluateWithObject:_email.text];
	BOOL filledFields = emailResult && [[_nome text] length] && [[_testo text] length];
	if (filledFields){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attenzione" message:[NSString stringWithFormat:@"Sicuro di voler inviare il messaggio \"%@\" ?",[_testo text]] delegate:self cancelButtonTitle:@"Annulla" otherButtonTitles:@"Invia", nil];
        [alert show];
	} else {
		if (!emailResult) 
			[[[UIAlertView alloc] initWithTitle:kWarning message:kErrorNotValidMail delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
		else
			[[[UIAlertView alloc] initWithTitle:kWarning message:kErrorNotFilledFiels delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
	}
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{

    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
	[[[UIAlertView alloc] initWithTitle:kWarning message:kErrorNotEndedRequest delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
	NSLog(@"Data received: \n %@",receivedData);
	
	[[[UIAlertView alloc] initWithTitle:kNotif message:kSmsSentNotification delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
	
	_testo.text = @"";
	
}


- (BOOL)textFieldShouldReturn:(id)sender{
	[sender resignFirstResponder];
	return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;{
	if ( [ text isEqualToString: @"\n" ] ) {
		[textView resignFirstResponder];
		return NO;
	}
	return YES;
}

@end
