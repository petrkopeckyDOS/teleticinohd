//
//  Teleticino_iPadAppDelegate.m
//  Teleticino_iPad
//
//  Created by Davide Vincenzi on 20.04.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Teleticino_iPadAppDelegate.h"

#import "HomeViewController.h"
#import "StatSendOperation.h"
#import "GAI.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@implementation Teleticino_iPadAppDelegate


@synthesize window=_window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    
    // Initialize tracker.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-47604344-1"];
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelWarning];
    
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-7833734746784267~9591565839"];
    
    avvii = 0;
    
    StatSendOperation *operation = [[StatSendOperation alloc] init];
    [operation sendStats];

    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    [commandCenter.pauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent *event) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopAudio" object:nil];
        
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    [commandCenter.playCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent *event) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playAudio" object:nil];
        
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    return YES;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    if (avvii == 0) [[NSNotificationCenter defaultCenter] postNotificationName:@"ricaricaTutto" object:nil];
    avvii ++;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

//Called before connection is established
//parameter:  nil
//return:     nil
-(void)pollStarted:(id)anObject{
    // NSLog(@"I'm going to get balls of steel");
}

//Called after connection finished
//parameter:  nil
//return:     nil
-(void)pollExecuted:(id)anObject{
    // NSLog(@"I've got balls of steel!");
}

//Called if connection fail
//parameter:  NSError 
//return:     nil
-(void)pollFailed:(NSError*)anObject{
    //  NSLog(@"I'll never get balls of steel cuz of : %@",[anObject description]);
}

@end
