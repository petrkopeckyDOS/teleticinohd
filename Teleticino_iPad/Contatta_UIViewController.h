//
//  Contatta_UIViewController.h
//  TeleTicino
//
//  Created by Antonio Egizio on 07/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+StringFix.h"


@interface Contatta_UIViewController : UIViewController <UITextViewDelegate, UIAlertViewDelegate>{
	
	NSMutableData *receivedData;
	BOOL isUp;
	
	id editingField;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UITextField *nome;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextView *testo;
@property (weak, nonatomic) IBOutlet UIButton *invio;


-(IBAction)inviaButtonTouchUpInside:(id)sender;
-(void)dismissKeyboard:(id)sender;
-(IBAction)fieldDidBeginEditing:(id)sender;
-(IBAction)clear:(id)sender;

@end
