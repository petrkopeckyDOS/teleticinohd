//
//  StreamingViews.h
//  TeleTicino
//
//  Created by Antonio Egizio on 09/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Radio3i_UIViewController.h"


@interface StreamingViews : NSObject {
	
}

@property (nonatomic, strong) NSMutableDictionary *streamingViewsList;

+ (StreamingViews *)sharedInstance;

-(void) closeAllPlayingViewBut:(id)thisView;
-(void) closeStoppedStreamingViews;
-(id) getObjectForClass:(id)streamingViewControllerClass;
-(void)addStreamingView:(id)streamingView ofClass:(id)streamingViewClass;


@end