//
//  StreamingViewControllerCommon.m
//  TeleTicino
//
//  Created by Antonio Egizio on 07/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "StreamingViewControllerCommon.h"


@implementation StreamingViewControllerCommon

@synthesize isMusicStopped;

-(void) destroyStreamer {
    
}

-(void) closeStream {
	[streamer stop];
}

@end
