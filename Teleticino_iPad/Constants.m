//
//  Constants.m
//  ticinonews
//
//  Created by Petr Kopecky on 30.03.17.
//  Copyright © 2017 Dos Informatica ed Elettronica Sagl. All rights reserved.
//

#import <Foundation/Foundation.h>


NSString *const VIDEO_AD_TAG = @"https://pubads.g.doubleclick.net/gampad/ads?sz=1024x576&iu=/8373/CH/timedia/CH_Ticinonews.ch-Internet-TV_EX_Services/PreRoll/IT_PreRoll&ciu_szs=&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]";

NSString *const AD_ID = @"/8373/CH/timedia/CH_Ticinonews.ch-Tablet_EX_Services/iOS/IT_iOS";

