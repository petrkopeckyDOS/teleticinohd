//
//  Telegiornali_UIViewController.h
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "DataAccess.h"
#import "IconDownloader.h"
#import "DOSMoviePlayerController.h"

@protocol Podcast_UIViewControllerDelegate;

@interface Podcast_UIViewController : UIViewController <IconDownloaderDelegate,UIWebViewDelegate>  {
	DataAccess *dataAccess;
	NSMutableArray *arrayDati;
    NSMutableDictionary *imageDownloadsInProgress;
    
}

@property (nonatomic, strong) DOSMoviePlayerController *moviePlayerView;
@property (nonatomic, weak) id <Podcast_UIViewControllerDelegate> delegate;
@property (nonatomic,weak) IBOutlet UITableView *table;
@property (nonatomic,strong) DataAccess *dataAccess;
@property (nonatomic,strong) NSMutableArray *arrayDati;
@property (nonatomic,weak) IBOutlet UITableViewCell *miaCell;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;

- (void)appImageDidLoad:(NSIndexPath *)indexPath;

@end

@protocol Podcast_UIViewControllerDelegate <NSObject>

- (void)podcastViewControllerShouldDismiss:(Podcast_UIViewController *)podcastVC;

@end