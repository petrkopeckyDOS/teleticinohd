//
//  Info_UIViewController.m
//  TeleTicino
//
//  Created by Antonio Egizio on 24/03/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "Info_UIViewController.h"


@implementation Info_UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc{   
    [_infoWeb stopLoading];
}

- (void)didReceiveMemoryWarning{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    
    StatSendOperation *stats = [[StatSendOperation alloc] init];
    [stats sendStats];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.firstLoad = YES;
    _infoWeb.dataDetectorTypes = UIDataDetectorTypeNone;
    _infoWeb.delegate = self;
    NSMutableString *url = [[NSMutableString  alloc] initWithString:@"http://appcenter.dos-group.com/?"];
    [url appendString:[self moreInfoInURLs]];
    [_infoWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    [_loading startAnimating];
}

-(NSString*)moreInfoInURLs {
    NSMutableString *moreInfoString = [[NSMutableString alloc] init];
    [moreInfoString appendFormat:@"AppName=%@&", kAppName];
    [moreInfoString appendFormat:@"DeviceName=%@&", [[UIDevice currentDevice] name]];
    [moreInfoString appendFormat:@"AppVersion=%@&", [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey]];
    [moreInfoString appendFormat:@"OsName=%@&", [[UIDevice currentDevice] systemName]];
    [moreInfoString appendFormat:@"OsVersion=%@&", [[UIDevice currentDevice] systemVersion]];
    [moreInfoString appendFormat:@"AppId=%@&", [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleIdentifierKey]];
    
    NSMutableString *languages = [[NSMutableString alloc] initWithString:@"Lang="];
    int i = 0;
    for (NSString *language in [NSLocale preferredLanguages]) {
        [languages appendFormat:@"%@,", language];
        i++;
        if (i == 3) break;
    }
    [moreInfoString appendString:[languages substringToIndex:[languages length] - 1]];
    
    return [moreInfoString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)exitPressed:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - WebView Delegate methods

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.firstLoad = NO;
    [_loading stopAnimating];
    [webView setHidden:NO];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [_loading stopAnimating];
    [webView setHidden:NO];
    NSString *html = [[NSString alloc] initWithFormat:@"<html><body> Error loading info page, error specification: <br /> %@ <br /></body></html>",[error localizedDescription]];
    [webView loadHTMLString:html baseURL:nil];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (_firstLoad) return YES;
	NSURL* url = [request URL];
	if (UIWebViewNavigationTypeLinkClicked == navigationType){
		[[UIApplication sharedApplication] openURL:url];
		return NO;
	}
	if([[[request URL] absoluteString] rangeOfString:@"mailto:"].location != NSNotFound) 
		return YES;
	return NO;
}

@end
