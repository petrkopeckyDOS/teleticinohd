//
//  TeleTicino_UIViewController.m
//  TeleTicino-iPad
//
//  Created by Antonio Egizio on 16/03/11.
//  Copyright 2011 Dos Informatica ed Elettronica Sagl. All rights reserved.
//

#import "TeleTicino_UIViewController.h"
#import "Radio3i_UIViewController.h"
#import "Teleticino_iPadAppDelegate.h"
#import "HomeViewController.h"
#import "Palinsesto_UIViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVkit.h>
#import <GoogleInteractiveMediaAds/GoogleInteractiveMediaAds.h>

//#define buttonWidth 157
#define buttonHeight 38

@interface TeleTicino_UIViewController() <IMAAdsLoaderDelegate, IMAAdsManagerDelegate>

@property (nonatomic, strong) AVPlayerViewController *moviePlayerController;

#pragma mark - Ad
@property(nonatomic, strong) IMAAdsLoader *adsLoader;
@property(nonatomic, strong) IMAAVPlayerContentPlayhead *contentPlayhead;
@property(nonatomic, strong) IMAAdsManager *adsManager;

#pragma mark - AVPlayer
@property(nonatomic, strong) AVPlayer *player;

@end

@implementation TeleTicino_UIViewController

- (void) reloadData
{
    [_loading startAnimating];
    
    if(_arrayDati)
        [_arrayDati removeAllObjects];
    
    if (_imageList)
        [_imageList removeAllObjects];
    
    for (UIView *subview in [_videoContainer subviews]) {
        [subview removeFromSuperview];
    }
    
    for (UIView *subView in [[self.view viewWithTag:1] subviews])
    {
        if (subView.tag == 345) [subView removeFromSuperview];
    }
    
    if ([_selected isEqualToString:@"Palinsesto"])
    {
        Palinsesto_UIViewController *palVC = [[Palinsesto_UIViewController alloc] initWithNibName:@"Palinsesto_UIViewController" bundle:nil];
        [self addChildViewController:palVC];
        [palVC.view setTag:345];
        CGRect frame = CGRectMake(0, 0, [self.view viewWithTag:1].frame.size.width, [self.view viewWithTag:1].frame.size.height);
        [palVC.view setFrame:frame];
        [[self.view viewWithTag:1] addSubview:palVC.view];
        self.isLoading = NO;
    } else if ([_selected isEqualToString:@"diretta"]) {
        AppRecord *record = [[AppRecord alloc] init];
        [record setImageURLString:[[NSBundle mainBundle] pathForResource:@"diretta_placeholder" ofType:@"png"]];
        [record setImageNameFallback:@"direttaTVPlaceholder"];
        [record setTitle:@"Guarda la diretta"];
        [record setSubtitle:@""];
        [record setDetailURLString:@"http://livevideo.infomaniak.com/streaming/livecast/teleticinolive/playlist.m3u8"];
        
        self.arrayDati = [NSMutableArray arrayWithObject:record];
        [self loadPreview];
    } else {
        _descriptionLabel.text = @"Caricamento...";
        
        [_videoContainer setContentSize:CGSizeZero];
        
        self.dataAccess = [[DataAccess alloc] init];
        
        [_dataAccess SetItemsReceivedSelector:self :@selector(dataReload:)];
        [_dataAccess GetTeleTicinoData:_selected];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (_arrayDati)
        [_arrayDati removeAllObjects];
    
    if (_selected){
        self.selected = nil;
    }
    if (_selectedNodeName){
        self.selectedNodeName = nil;
    }
}

- (void)didReceiveMemoryWarning{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark -
#pragma mark Populate View

#define ELEMENTI 7
-(void) dataReload:(NSMutableData *)data {
    if (!data){
        [self.loading stopAnimating];
        self.isLoading = NO;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Impossibile contattare il server, connettività limitata o assente. Verifica la tua connessione a Internet e riprova. Se il problema persiste riprova più tardi." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    TBXML *tbxml = [TBXML tbxmlWithXMLData:data];
	NSMutableArray *temp = [Common XMLGetItemsBelowNodeName:tbxml.rootXMLElement NodeName:_selectedNodeName];
	
	AppRecord *appRecord = nil;
	int j = 0;
	int x = 0;
	
	for (int i = 0; i < [temp count]; i++) {
		if (j == 0)	{
			++x;
			if ([_arrayDati count] < x) {
				appRecord = [[AppRecord alloc] init];	
			} else {
				appRecord = _arrayDati[(x - 1)];
			}
		}
		if (j == 0) {
			
			appRecord.pubDate = ((ItemKeyValue *)temp[i]).nodeValue;
		}	
		if (j == 1) {
			appRecord.title = ((ItemKeyValue *)temp[i]).nodeValue;
		}	
		if (j == 2) {
            appRecord.subtitle = ((ItemKeyValue *)temp[i]).nodeValue;
		}	
		if (j == 3) {
            appRecord.imageURLString = [((ItemKeyValue *)temp[i]).nodeValue htmlUnescapedString];
            NSLog(@"%@", appRecord.imageURLString);
            NSString *textToCut = @"_rett";
            NSRange cutLocation = [appRecord.imageURLString rangeOfString:textToCut];
            if (cutLocation.location > 0){
                NSString *newImageUrl = [[NSString alloc] initWithFormat:@"%@_header.jpg",[appRecord.imageURLString substringToIndex:cutLocation.location]];
                appRecord.imageURLString = [newImageUrl copy];
            }
			
		}	
		if (j == 4) {
			appRecord.detailURLString = ((ItemKeyValue *)temp[i]).nodeValue;
		}
		
		++j;
		if (j == ELEMENTI)	{
			if ([_arrayDati count] < x) {
				[_arrayDati addObject:appRecord];
			} 
			j = 0;
		}
	}
    
    [self loadPreview];
}

- (void)nonBlockingLoading:(id)sender{
    for (AppRecord* rec in _arrayDati){
        if (![rec.detailURLString isEqualToString:@""]){
            NSLog(@"%@",rec.detailURLString);
            MPMoviePlayerController *movie = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:rec.detailURLString]];
            UIImage *preview = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:rec.imageURLString]]];; // MPMovieTimeOptionNearestKeyFrame
            
            [movie stop];
            if (!preview) {
                preview = [UIImage imageNamed:rec.imageNameFallback];
            }
            if (preview) {
                NSLog(@"Added image");
                _imageList[rec.detailURLString] = preview;
            } else {
                UIImage *image = [UIImage imageWithContentsOfFile:rec.imageURLString];
                if (image) {
                    _imageList[rec.detailURLString] = image;
                }
                
            }
        }
    }
    [self showPreview];
}
/*
- (void)stop:(UIButton*)sender{
    [sender.superview removeFromSuperview];
}
*/

- (void)playTouched:(id)sender{
    [self setupAdsLoader];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopAudio" object:nil];
    
    NSInteger tag = ((UIButton*)sender).tag;
    NSString *sVideoUrl = ((VideoContainerItem_UIView *)[_videoContainer viewWithTag:tag]).detail;
    
    self.player = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:sVideoUrl]];
    self.moviePlayerController = [[AVPlayerViewController alloc] init];
    self.moviePlayerController.player = self.player;
    self.contentPlayhead = [[IMAAVPlayerContentPlayhead alloc]initWithAVPlayer:self.player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(contentDidFinishPlaying:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.player.currentItem];

    [self adRequest];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [self presentViewController:self.moviePlayerController animated:YES completion:nil];
    // Enable the type of linear ads you want presented.

    
    // Set the configuration on the movie player instance.


   
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playIt:) name:MPMoviePlayerLoadStateDidChangeNotification object:playMovie];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(impostaStatusBar) name:MPMoviePlayerDidEnterFullscreenNotification object:nil];
}


#pragma mark - Ad

- (void)setupAdsLoader {
    if (self.adsLoader) {
        self.adsLoader = nil;
    }
    IMASettings *settings = [[IMASettings alloc] init];
    settings.enableBackgroundPlayback = YES;
    self.adsLoader = [[IMAAdsLoader alloc] initWithSettings:settings];
    self.adsLoader.delegate = self;
}

-(void)adRequest {
    
    IMAAdDisplayContainer *adDisplayContainer = [[IMAAdDisplayContainer alloc] initWithAdContainer:self.moviePlayerController.view companionSlots:nil];
    // Create an ad request with our ad tag, display container, and optional user context.
    IMAAdsRequest *request = [[IMAAdsRequest alloc] initWithAdTagUrl:VIDEO_AD_TAG
                                                  adDisplayContainer:adDisplayContainer
                                                     contentPlayhead:self.contentPlayhead
                                                         userContext:nil];
    [self.adsLoader requestAdsWithRequest:request];
}

-(void)contentDidFinishPlaying:(NSNotification *)notification {
    
    if (notification.object == self.player.currentItem) {
        [self.adsLoader contentComplete];
    }
}

#pragma mark AdsLoader Delegates

- (void)adsLoader:(IMAAdsLoader *)loader adsLoadedWithData:(IMAAdsLoadedData *)adsLoadedData {
    self.adsManager = adsLoadedData.adsManager;
    self.adsManager.delegate = self;
    IMAAdsRenderingSettings *adsRenderingSettings = [[IMAAdsRenderingSettings alloc] init];
    adsRenderingSettings.webOpenerPresentingController = self;
    [self.adsManager initializeWithAdsRenderingSettings:adsRenderingSettings];
}

- (void)adsLoader:(IMAAdsLoader *)loader failedWithErrorData:(IMAAdLoadingErrorData *)adErrorData {
    NSLog(@"Error loading ads: %@", adErrorData.adError.message);
    [self.player play];
}

#pragma mark AdsManager Delegates

- (void)adsManager:(IMAAdsManager *)adsManager didReceiveAdEvent:(IMAAdEvent *)event {
    if (event.type == kIMAAdEvent_LOADED) {
        [adsManager start];
    }
}

- (void)adsManager:(IMAAdsManager *)adsManager didReceiveAdError:(IMAAdError *)error {
    NSLog(@"AdsManager error: %@", error.message);
    [self.player play];
}

- (void)adsManagerDidRequestContentPause:(IMAAdsManager *)adsManager {
    [self.player pause];
}

- (void)adsManagerDidRequestContentResume:(IMAAdsManager *)adsManager {
    [self.player play];
}

- (void) impostaStatusBar
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
}

/*
- (void)playIt:(NSNotification *)notification{
    MPMoviePlayerController *moviePlayerController = [notification object];
    [moviePlayerController play];
    UIButton *quit = (UIButton*)[moviePlayerController.view viewWithTag:100];
    quit.enabled = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:moviePlayerController];
}
 */

- (void)moviePlaybackComplete:(NSNotification *)notification{
    MPMoviePlayerController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayerController];
    [moviePlayerController.view removeFromSuperview];
    
//    Teleticino_iPadAppDelegate *appDelegate = (Teleticino_iPadAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [appDelegate.viewController setupViewWithOrientation];
}

#define kOffset 40

- (void)showPreview{
    
    for (UIView *subview in [_videoContainer subviews]) {
        [subview removeFromSuperview];
    }
    
    for (UIView *subView in [[self.view viewWithTag:1] subviews])
    {
        if (subView.tag == 345) [subView removeFromSuperview];
    }
    
    int i = 3; //Start from 3 to avoid matching 0 tag default configuration
    int x = (_videoContainer.frame.size.width / 2) - (kWidth / 2);
    int y = 20;
    
    [_videoContainer setContentSize:CGSizeMake((kWidth + ([_imageList count] * kWidth * sin(kInclination) * kScale / 2)) + x + kOffset * ([_imageList count] -1)+100, _videoContainer.frame.size.height)];
    
    NSLog(@"%f",_videoContainer.contentSize.width);
    
    for (NSString *videoUrl in _imageList){
        UIImage* preview = _imageList[videoUrl];
        VideoContainerItem_UIView *coverFlow = [[VideoContainerItem_UIView alloc] initWithFrame:CGRectMake(x, y, 0, 0)];
        [coverFlow setThumbnailImage:preview];
        coverFlow.tag = i;
        coverFlow.detail = [[NSString alloc] initWithString:videoUrl];
    
        UIButton *playButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [playButton setImage:[UIImage imageNamed:@"playButton"] forState:UIControlStateNormal];
        [playButton addTarget:self action:@selector(playTouched:) forControlEvents:UIControlEventTouchUpInside];
        
        coverFlow.utilityButton = playButton;
        
        
        if (i==3){
            [coverFlow xcenter];
             _descriptionLabel.text = [self getDescForVideoUrl:coverFlow.detail];
            x+= coverFlow.frame.size.width * sin(kInclination) * kScale / 2 + kOffset - 20;
        }
        else{
            [coverFlow xright];
            x += coverFlow.frame.size.width / 2 + kOffset; //new origin = old origin + (previous cover flow size / 2) + offset
            NSLog(@"%f",coverFlow.frame.size.width / 2 + kOffset);
        }
        i++;
        [_videoContainer addSubview:coverFlow];
               
    }
    
    maxTag = i;
    [_loading stopAnimating];
    
    self.isLoading = NO;
}

- (void)loadPreview{
    [NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(nonBlockingLoading:) userInfo:nil repeats:NO]; //Started after 0.1 ms
    
}

- (void) stopPlayer
{
    //if (_moviePlayerController.playbackState != MPMoviePlaybackStateStopped){
        [self performSelector:@selector(removePlayer) withObject:nil afterDelay:0.0];
    //}
}

-(void)rubricaTabButtonTouchUpInside:(id)sender{
	if (((UIButton*)sender).tag != selectedButton && !_isLoading) {
        self.isLoading = YES;
        
        //Stoppiamo l'eventuale video

		//[super rubricaTabButtonTouchUpInside:sender];	
        
        
        UIButton *selectedBut = _headings[selectedButton];
        selectedBut.selected = NO;
        ((UIButton*)sender).selected = YES;
        
        selectedButton = ((UIButton*)sender).tag;
        
        [selectedBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        selectedBut.titleLabel.font = [UIFont boldSystemFontOfSize: 14];
        selectedBut.selected = NO;
        [selectedBut setBackgroundColor:[UIColor clearColor]];
        
        ((UIButton*)sender).selected = YES;
        [((UIButton*)sender) setBackgroundColor:[UIColor whiteColor]];
        [((UIButton*)sender) setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        ((UIButton*)sender).titleLabel.font = [UIFont boldSystemFontOfSize: 14];
        
        if (((UIButton *)sender).tag == 0)
        {
            self.selected = @"diretta";
        } else if (((UIButton *)sender).tag == 1)
        {
            self.selected = @"servizi";
        }
		else if (((UIButton *)sender).tag == 2)
        {
            self.selected = @"ReplayProgrammi";
        }
		else if (((UIButton *)sender).tag == 3)
        {
            self.selected = @"Palinsesto";
        }
        [self reloadData];
	}
}

- (void)addHeading:(NSString*)aHeading{
	if (!_headings) {
		self.headings = [[NSMutableArray alloc] initWithCapacity:0];
        self.headingWidth = 0.0;
    }
	UIButton *button = [[UIButton alloc]initWithFrame:CGRectZero];
    
    CGFloat buttonWidth = 0.0;
    if ([aHeading isEqualToString:@"DIRETTA TV"]) {
        [button setImage:[UIImage imageNamed:@"menu_direttatv_bianco"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"menu_direttatv_grigio"] forState:UIControlStateSelected];
        buttonWidth = 102.0;
    } else if ([aHeading isEqualToString:@"SERVIZI TG"]) {
        [button setImage:[UIImage imageNamed:@"menu_servizitg_bianco"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"menu_servizitg_grigio"] forState:UIControlStateSelected];
        buttonWidth = 105.0;
    } else if ([aHeading isEqualToString:@"REPLAY PROGRAMMI"]) {
        [button setImage:[UIImage imageNamed:@"menu_replay_bianco"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"menu_replay_grigio"] forState:UIControlStateSelected];
        buttonWidth = 168.0;
    } else if ([aHeading isEqualToString:@"PALINSESTO"]) {
        [button setImage:[UIImage imageNamed:@"menu_palinsesto_bianco"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"menu_palinsesto_grigio"] forState:UIControlStateSelected];
        buttonWidth = 115.0;
    }
    
    buttonWidth = buttonWidth + 5.0;
    [button setFrame:CGRectMake(_headingWidth + 1, 0, buttonWidth, buttonHeight)];
    
    self.headingWidth = _headingWidth + buttonWidth;
	
//    [button setTitle:aHeading forState: UIControlStateNormal];
//	button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    button.titleLabel.font = [UIFont boldSystemFontOfSize: 14];
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.titleLabel.shadowOffset    = CGSizeMake (0.0, 0.0);
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
	button.tag = [_headings count];
	
    if (button.tag == (selectedButton)) {
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize: 14];
        [button setBackgroundColor:[UIColor whiteColor]];
	} else {
        [button setBackgroundColor:[UIColor clearColor]];
    }
    
	[button addTarget:self action:@selector(rubricaTabButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
	
    [_headings addObject:button];
}

#pragma mark - View lifecycle

- (void) ricarica
{
    if (!_isLoading) [self reloadData];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self.view.layer setBorderColor:[UIColor colorWithRed:190.0/255.0 green:190.0/255.0 blue:190.0/255.0 alpha:1.0].CGColor];
    [self.view.layer setBorderWidth:1.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayer) name:@"stopVideo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ricarica) name:@"ricaricaTutto" object:nil];

    _headingTab.contentSize = CGSizeMake(90 * [_headings count] + PAD, 49);
    
	for (UIButton* button in _headings)
    {
		[_headingTab addSubview:button];
		if (button.tag == selectedButton)
			button.selected = YES;
	}
    
    //self.radio = [[Radio3i_UIViewController alloc] initWithNibName:@"Radio3i_UIViewController" bundle:nil];
    //[self.radioView addSubview:_radio.view];
        
    self.imageList = [[NSMutableDictionary alloc] initWithCapacity:0];
    self.arrayDati = [[NSMutableArray alloc] init];
	self.dataAccess = [[DataAccess alloc] init];
    self.selected = @"diretta";
    self.selectedNodeName = @"servizio";
    
}

- (void) removePlayer {

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self lockOnCentered:NO];
}

#pragma mark -
#pragma mark Scroll Delegate

- (void)fixViewError{
    BOOL found = NO;
    for (int i=3; i<=maxTag; i++){
        VideoContainerItem_UIView *coverFlow = (VideoContainerItem_UIView *)[_videoContainer viewWithTag:i];
        if (found && coverFlow.flowState == UIFlowStateCenter)
            [coverFlow xright];
        found = !found?coverFlow.flowState == UIFlowStateCenter:found;
    }
}

-(NSString*)getDescForVideoUrl:(NSString*)aVideoUrl{
    for (int i=0; i<[_arrayDati count]; i++)
        if ([((AppRecord*)_arrayDati[i]).detailURLString isEqualToString:aVideoUrl])
            return ((AppRecord*)_arrayDati[i]).title;
    return @"Video";
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    UIScrollDirection scrollDirection = UIScrollDirectionNone;
    if (lastContentOffset > scrollView.contentOffset.x)
        scrollDirection = UIScrollDirectionRight;
    else if (lastContentOffset < scrollView.contentOffset.x) 
        scrollDirection = UIScrollDirectionLeft;
    
    lastContentOffset = scrollView.contentOffset.x;
    
    float animationTime = 0.3;
    
    
    float deltaOffset = abs(scrollView.contentOffset.x - preoffset);
    float deltaTime = CACurrentMediaTime();
    preoffset = scrollView.contentOffset.x;
    
    float speed = deltaOffset / deltaTime;
    
    NSLog(@"%f",speed);
    
    UIScrollingSpeed speedEn = (speed < 0.000252) ? UIScrollingSpeedNormal : UIScrollingSpeedFast;
    
    switch (speedEn) {
        case UIScrollingSpeedNormal:
            animationTime = 0.3;
            break;
            
        case UIScrollingSpeedFast:
            animationTime = 0.1;
            break;
    }
    
    animationTime = 0.3;
    //animationTime = 1;
    
    switch (scrollDirection) {
        case UIScrollDirectionRight:
            for (int i=3; i<=maxTag; i++){
                VideoContainerItem_UIView *coverFlow = (VideoContainerItem_UIView *)[_videoContainer viewWithTag:i];
                if (coverFlow.flowState == UIFlowStateLeft && (coverFlow.frame.origin.x + coverFlow.frame.size.width) > (_videoContainer.frame.size.width / 2 + _videoContainer.contentOffset.x)){
                    VideoContainerItem_UIView *nextCoverFlow = (VideoContainerItem_UIView *)[_videoContainer viewWithTag:i+1];
                    [UIView beginAnimations:nil context:nil];
                    [UIView setAnimationDuration:animationTime];
                    [nextCoverFlow xright];
                    [coverFlow xcenter];
                    _descriptionLabel.text = [self getDescForVideoUrl:coverFlow.detail];
                    [UIView commitAnimations];
                    break;
                }
            }
            break;
        case UIScrollDirectionLeft:
            for (int i=3; i<=maxTag; i++){
                VideoContainerItem_UIView *coverFlow = (VideoContainerItem_UIView *)[_videoContainer viewWithTag:i];
                if (coverFlow.flowState == UIFlowStateRight && coverFlow.frame.origin.x < (_videoContainer.frame.size.width / 2 + _videoContainer.contentOffset.x)){
                    VideoContainerItem_UIView *previousCoverFlow = (VideoContainerItem_UIView *)[_videoContainer viewWithTag:i-1];
                    [UIView beginAnimations:nil context:nil];
                    [UIView setAnimationDuration:animationTime];
                    [previousCoverFlow xleft];
                    [coverFlow xcenter];
                    _descriptionLabel.text = [self getDescForVideoUrl:coverFlow.detail];
                    [UIView commitAnimations];  
                    break;
                }
            }
            break;
        default:
            break;
    }
    [self fixViewError];
}

- (void) lockOnCentered:(BOOL)decelerate{
    if (!decelerate){
        for (int i=3; i<=maxTag; i++){
            VideoContainerItem_UIView *coverFlow = (VideoContainerItem_UIView *)[_videoContainer viewWithTag:i];
            if (coverFlow.flowState == UIFlowStateCenter){
                CGRect scrollTo = CGRectMake(coverFlow.frame.origin.x - ((_videoContainer.frame.size.width / 2) - (kWidth / 2)), 0, _videoContainer.frame.size.width, _videoContainer.frame.size.height);
                [_videoContainer scrollRectToVisible:scrollTo animated:YES];
                break;
            }
        }
        [self fixViewError];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self lockOnCentered:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self lockOnCentered:NO];
}


#pragma mark -
#pragma mark Video List View Management

- (void)showNewsMovieList{
    if (_selected){
        self.selected = nil;
    }
    if (_selectedNodeName){
        self.selectedNodeName = nil;
    }
    
    self.selected = @"diretta";
    self.selectedNodeName = @"servizio";
    if(_arrayDati)
        [_arrayDati removeAllObjects];
    
    if (_imageList)
        [_imageList removeAllObjects];
    
    for(UIView *subview in [_videoContainer subviews]) {
        [subview removeFromSuperview];
    }
    
    _descriptionLabel.text = @"Caricamento...";
    
    [_videoContainer setContentSize:CGSizeZero];
	
	[_dataAccess SetItemsReceivedSelector:self :@selector(dataReload:)];
	[_dataAccess GetTeleTicinoData:_selected];
}

@end
