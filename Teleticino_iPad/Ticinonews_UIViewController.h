//
//  Ticinonews_UIViewController.h
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataAccess.h"
#import "IconDownloader.h"
#import "NewsTab_UIViewController.h"
#import "StreamingViews.h"
#import "Detail_UIViewController.h"

@protocol TopNewsDelegate;


@interface Ticinonews_UIViewController : UIViewController <IconDownloaderDelegate, UIWebViewDelegate> {
    
}

@property (nonatomic, unsafe_unretained) BOOL isLoading;
@property (nonatomic, strong) NewsTab_UIViewController *news;
@property (nonatomic, strong) NSArray *rubriche;
@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) DataAccess *dataAccess;
@property (nonatomic, strong) NSMutableArray *arrayDati;
@property (nonatomic, weak) IBOutlet UITableViewCell *miaCell;
@property (nonatomic, weak) IBOutlet UITableViewCell *topCell;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;
@property (nonatomic, unsafe_unretained) id delegate;

- (void)appImageDidLoad:(NSIndexPath *)indexPath;

@end
