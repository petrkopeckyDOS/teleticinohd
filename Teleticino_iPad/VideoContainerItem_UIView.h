//
//  VideoContainerItem_UIView.h
//  TeleTicino-iPad
//
//  Created by Antonio Egizio on 16/03/11.
//  Copyright 2011 Dos Informatica ed Elettronica Sagl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#define kWidth  293
#define kHeight 172
#define kScale 0.70f
#define kInclination 45.0f //do NOT change!
#define reflectionFraction 0.3
#define reflectionOpacity 0.7

enum {
    UIFlowStateCenter         = 0,                       
    UIFlowStateLeft           = 1,
    UIFlowStateRight          = 2
};
typedef NSUInteger UIFlowState;


@interface VideoContainerItem_UIView : UIView {
    UIImageView *thumbnail;
    
    NSString *detail;
    BOOL showReflect;
    UIFlowState flowState;
    UIButton* utilityButton;
}

@property (nonatomic,strong,setter = setThumbnail:) UIImageView *thumbnail;
@property (nonatomic,assign) BOOL showReflect;
@property (nonatomic,strong) NSString *detail;
@property (nonatomic, readonly) UIFlowState flowState;
@property (nonatomic,strong, setter = setUtilityButton:) UIButton* utilityButton;
@property (nonatomic, strong) UIImageView *localReflectionImageView;

- (void)xcenter;
- (void)xleft;
- (void)xright;
- (void)setThumbnail:(UIImageView *)aThumbnail;
- (void)setThumbnailImage:(UIImage*)anImage;
-(void)setUtilityButton:(UIButton *)aButton;

@end
