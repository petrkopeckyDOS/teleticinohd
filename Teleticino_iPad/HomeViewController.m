//
//  Teleticino_iPadViewController.m
//  Teleticino_iPad
//
//  Created by Davide Vincenzi on 20.04.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define kTopnews	@"Topnews"
#define kTicino		@"Ticino"
#define kSvizzera	@"Svizzera"
#define kEstero		@"Estero"
#define kSport		@"Sport"
#define kPodcast	@"Podcast"
#define kServizitg	@"Servizi TG"

#import "HomeViewController.h"
#import "NewsTab_UIViewController.h"
#import "TeleTicino_UIViewController.h"
#import "Radio3i_UIViewController.h"
#import "Info_UIViewController.h"
#import "Teleticino_iPadAppDelegate.h"

#import <GoogleMobileAds/GoogleMobileAds.h>

#define kBannerPortrait @"iOS_tablet_banner_portrait-5" //FUNZIONA
#define kBannerLandscape @"iOS_tablet_banner_landscape-5" //FUNZIONA


@interface HomeViewController () <GADInterstitialDelegate, GADBannerViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ticinonewsTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoTrailingSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radioLeadingSpaceConstraint;;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerWidth;

//Admob
@property (weak, nonatomic) IBOutlet UIView *bannerWrapperView;
@property (weak, nonatomic) IBOutlet UIView *topBannerWrapperView;
@property (nonatomic, strong) GADInterstitial *interstitial;
@property (strong, nonatomic) DFPBannerView *bannerView;
@property (strong, nonatomic)  DFPBannerView *topBannerView;
@property (strong, nonatomic) DFPRequest *request;

//Admob UI helper's
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBannerWrapperConstrainHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBannerWrapperConstrainWidth;
@property (weak, nonatomic) IBOutlet UIImageView *closeButtonImageView;

@end

@implementation HomeViewController

- (void) dismissAndRepresent
{
    UIViewController *controller = self.modalViewController;
    if (controller)
    {
        [self dismissModalViewControllerAnimated:NO];
        [self presentModalViewController:controller animated:YES];
    }
}

- (void) presentDetail:(Detail_UIViewController *)detail {
    [detail setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self presentViewController:detail animated:YES completion:nil];
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.modalPresentationCapturesStatusBarAppearance = NO;
    
    StatSendOperation *stats = [[StatSendOperation alloc] init];
    [stats sendStats];
    
    if (self.initialized == NO) {
        [self addSubviews];
        self.initialized = YES;
    }
    [self loadBanner];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self setupViewWithOrientation];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self updateViewConstraints];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showInterstitialAd];
    self.screenName = NSStringFromClass([self class]);
}


- (IBAction) ricaricaTutto
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ricaricaTutto" object:nil];
}

- (IBAction) showInfo:(UIButton *)sender{
    Info_UIViewController *infoVC = [[Info_UIViewController alloc] initWithNibName:@"Info_UIViewController" bundle:nil];
    if ([_popoverCont isPopoverVisible]){
        [_popoverCont dismissPopoverAnimated:YES];
    }
    self.popoverCont = [[UIPopoverController alloc] initWithContentViewController:infoVC];
    _popoverCont.popoverContentSize = CGSizeMake(320, 450);
    [_popoverCont presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

#pragma mark - Admob

-(DFPRequest *)request {
    if (_request == nil) {
        _request = [DFPRequest request];

    }
    return _request;
}

-(void)showInterstitialAd {
    if (!self.interstitial) {
        self.interstitial = [[GADInterstitial alloc]initWithAdUnitID: AD_ID];
        self.interstitial.delegate = self;
        [self.interstitial loadRequest:self.request];
    }
}

-(void)loadBanner {
    self.bannerView = [[DFPBannerView alloc]initWithFrame:CGRectZero];
    self.topBannerView = [[DFPBannerView alloc]initWithFrame:CGRectZero];
    [self setBannerAdSize];
    
    for (DFPBannerView *banner in @[self.bannerView, self.topBannerView]) {
        banner.adUnitID = AD_ID;
        banner.delegate = self;
        banner.rootViewController = self;
        [banner loadRequest:self.request];
    }
}

-(void)setBannerAdSize {
    if ([[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeLeft
        ||[[UIDevice currentDevice]orientation] == UIDeviceOrientationLandscapeRight) {
        self.bannerView.adSize = kGADAdSizeSmartBannerLandscape;
    }
    else {
        self.bannerView.adSize = kGADAdSizeSmartBannerPortrait;
    }
    self.topBannerView.validAdSizes = @[NSValueFromGADAdSize(kGADAdSizeMediumRectangle), NSValueFromGADAdSize(GADAdSizeFromCGSize(CGSizeMake(600, 500)))];
}

#pragma mark - GADBanner delegate


-(void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    if (bannerView == self.topBannerView) {
        self.topBannerWrapperConstrainWidth.constant = bannerView.frame.size.width;
        self.topBannerWrapperConstrainHeight.constant = bannerView.frame.size.height;
        [self.topBannerWrapperView addSubview:self.topBannerView];
        self.closeButtonImageView.hidden = NO;
        [self.topBannerWrapperView addSubview:self.closeButtonImageView];
 
    }
    else {
        [self.bannerWrapperView addSubview:bannerView];
    }
    [bannerView.superview setHidden:NO];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed load banner with error: %@", [error localizedDescription]);
    if (bannerView == self.topBannerView) {
        [self.topBannerWrapperView setHidden:YES];
    }
  
}

#pragma mark - GADInterestial delegate

-(void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    }
}
-(void)interstitialDidFailToPresentScreen:(GADInterstitial *)ad {
    NSLog(@"Failed to present interstitial ad");
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to show interstitial ad with error: %@", error.localizedDescription);
}

#pragma mark - Tap gesture close button

- (IBAction)closeButtonTapped:(UITapGestureRecognizer *)sender {
    [UIView animateWithDuration:0.3 animations:^{
          self.topBannerWrapperView.hidden = YES;
    }];
    [UIView commitAnimations];
}

#pragma mark - UI setup

-(void)updateViewConstraints {
    [super updateViewConstraints];
    
    if (self.view.bounds.size.height < self.view.bounds.size.width) {
        _videoTrailingSpaceConstraint.constant = 382.0;
        _radioLeadingSpaceConstraint.constant = 374.0;
        _ticinonewsTopSpaceConstraint.constant = 99.0;
    } else {
        _videoTrailingSpaceConstraint.constant = 10.0;
        _radioLeadingSpaceConstraint.constant = floorf((_bottomView.frame.size.width - _pubblicitaView.frame.size.width) / 2.0);
        _ticinonewsTopSpaceConstraint.constant = floorf(_bottomView.frame.origin.y);
    }
    
    {
        UIView *ticinonewsView = [_ticinoNewsView viewWithTag:777];
        CGRect frame = ticinonewsView.frame;
        frame.size.height = _ticinoNewsView.frame.size.height;
        [ticinonewsView setFrame:frame];
    }
    
    {
        UIView *videoView = [_topView viewWithTag:777];
        CGRect frame = videoView.frame;
        frame.size.width = _topView.frame.size.width;
        [videoView setFrame:frame];
    }
    
    {
        UIView *topNewsView = [_bottomView viewWithTag:777];
        CGRect frame = topNewsView.frame;
        frame.size.height = _bottomView.frame.size.height;
        frame.size.width = _bottomView.frame.size.width;
        [topNewsView setFrame:frame];
    }
    
    {
        UIView *radioView = [_pubblicitaView viewWithTag:777];
        CGRect frame = radioView.frame;
        frame.size.width = _pubblicitaView.frame.size.width;
        [radioView setFrame:frame];
    }
}

- (void) setupViewWithOrientation {
    
    
    /*    if (self.interfaceOrientation == UIDeviceOrientationPortrait || self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown) {
     //Portrait
     
     _topView.frame = CGRectMake(8, 44, 752, 339);
     [[_topView viewWithTag:777] setFrame:CGRectMake(0, 0, 752, 339)];
     [[[_topView viewWithTag:1] viewWithTag:843] setFrame:CGRectMake(0, 0, 752, 290)];
     [[[_topView viewWithTag:1] viewWithTag:345] setFrame:CGRectMake(0, 0, 752, 339)];
     
     _bottomView.frame = CGRectMake(16, 430, 360, 255); //La foto � 613 x 278 (sempre?)
     
     _pubblicitaView.frame = CGRectMake(60, 700, 272, 255);
     
     _ticinoNewsView.frame = CGRectMake(392, 381, 360, 500);
     [[_ticinoNewsView viewWithTag:1] setFrame:CGRectMake(0, 0, 360, 100)]; //view
     } else if (self.interfaceOrientation == UIDeviceOrientationLandscapeLeft || self.interfaceOrientation == UIDeviceOrientationLandscapeRight) {
     //Landscape
     
     _topView.frame = CGRectMake(8, 44, 640, 339);
     [[_topView viewWithTag:777] setFrame:CGRectMake(0, 0, 640, 339)];
     [[[_topView viewWithTag:1] viewWithTag:843] setFrame:CGRectMake(0, 0, [_topView viewWithTag:1].frame.size.width, [_topView viewWithTag:1].frame.size.height)];
     [[[_topView viewWithTag:1] viewWithTag:345] setFrame:CGRectMake(0, 0, 640, 339)];
     
     _pubblicitaView.frame = CGRectMake(375, 420, 272, 255);
     
     _bottomView.frame = CGRectMake(8, 420, 360, 255); //widht : height = 613 : 278
     
     _ticinoNewsView.frame = CGRectMake(656, 84, 360, 640);
     [[_ticinoNewsView viewWithTag:1] setFrame:CGRectMake(0, 0, 360, 640)]; //view
     }
     */
}

- (void)addSubviews {
    
    //Impostiamo tutte le subview
    NewsTab_UIViewController *tempViewController = [[NewsTab_UIViewController alloc] initWithNibName:@"NewsTab_UIViewController" bundle:nil];
    [self addChildViewController:tempViewController];
    tempViewController.selected = 0;
    NSArray *rubriche = @[kTicino,kSvizzera,kEstero,kSport];
    for (NSString *section in rubriche){
        [tempViewController addHeading:section];
        NSLog(@"%@",section);
    }
    [tempViewController setDelegate:self];
    [_ticinoNewsView addSubview:tempViewController.view];
    
    TeleTicino_UIViewController *teleTicinoViewController = [[TeleTicino_UIViewController alloc] initWithNibName:@"TeleTicino_UIViewController" bundle:nil];
    [self addChildViewController:teleTicinoViewController];
    [teleTicinoViewController addHeading:@"DIRETTA TV"];
    [teleTicinoViewController addHeading:@"SERVIZI TG"];
    [teleTicinoViewController addHeading:@"REPLAY PROGRAMMI"];
    [teleTicinoViewController addHeading:@"PALINSESTO"];
    [teleTicinoViewController reloadData];
    
    [_topView addSubview:teleTicinoViewController.view];
    
    Ticinonews_UIViewController *ticinoNewsViewController = [[Ticinonews_UIViewController alloc] initWithNibName:@"Ticinonews_UIViewController" bundle:nil];
    [ticinoNewsViewController setDelegate:self];
    [self addChildViewController:ticinoNewsViewController];
    [_bottomView addSubview:ticinoNewsViewController.view];
    
    Radio3i_UIViewController *radioVC = [[Radio3i_UIViewController alloc] initWithNibName:@"Radio3i_UIViewController" bundle:nil];
    [self addChildViewController:radioVC];
    [_pubblicitaView addSubview:radioVC.view];
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    [self setupViewWithOrientation];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [self.view layoutSubviews];
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        self.bannerView.adSize = kGADAdSizeSmartBannerLandscape;
        [self.bannerView loadRequest:self.request];
    }
    else {
        self.bannerView.adSize = kGADAdSizeSmartBannerPortrait;
        [self.bannerView loadRequest:self.request];
    }
    
}

@end
