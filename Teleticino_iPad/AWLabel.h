//
//  AWRegularLabel.h
//  1trip2 iOS
//
//  Created by Davide Vincenzi on 13.01.14.
//  Copyright (c) 2014 AugmentedWorks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWRegularLabel : UILabel

@end


@interface AWBoldLabel : UILabel

@end



@interface AWRegularButton : UIButton

@end


@interface AWBoldButton : UIButton

@end