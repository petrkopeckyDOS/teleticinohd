//
//  DOSMoviePlayerController.h
//  Teleticino_iPad
//
//  Created by Davide on 04.02.14.
//
//

#import <MediaPlayer/MediaPlayer.h>

@interface DOSMoviePlayerController : MPMoviePlayerViewController

@property (nonatomic, unsafe_unretained) UIInterfaceOrientationMask orientationMask;

@end
