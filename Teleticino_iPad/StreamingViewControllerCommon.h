//
//  StreamingViewControllerCommon.h
//  TeleTicino
//
//  Created by Antonio Egizio on 07/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioStream.h"


@interface StreamingViewControllerCommon : UIViewController {
	AudioStream *streamer;
	BOOL isMusicStopped;
}
@property (nonatomic, readonly) BOOL isMusicStopped;

-(void) destroyStreamer;
-(void) closeStream;

@end
