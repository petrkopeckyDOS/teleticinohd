//
//  NSString+StringFix.h
//  TeleTicino
//
//  Created by Antonio Egizio on 18/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (StringFix) 

-(NSString*)stringByRemovingAccents;
-(NSString *)stringByFixingNonValidCharacters;
- (NSString *)htmlEscapedString;
- (NSString *)htmlUnescapedString;

@end