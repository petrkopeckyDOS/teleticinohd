//
//  Playlist_UIViewController.h
//  TeleTicino
//
//  Created by Antonio Egizio on 09/03/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataAccess.h"
#import "Common.h"
#import "AppRecord.h"

@interface Playlist_UIViewController : UIViewController <UITableViewDelegate, UIWebViewDelegate>{
    DataAccess *dataAccess;
    NSMutableArray *arrayDati;
    BOOL isDOWNLOADING;
    NSTimer *timer;
    NSMutableData* receivedData;
}


@property (nonatomic,weak) IBOutlet UITableView *table;
@property (nonatomic,strong) DataAccess *dataAccess;
@property (nonatomic,strong) NSMutableArray *arrayDati;
@property (nonatomic,weak) IBOutlet UITableViewCell *miaCell;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *loading;

- (void)refreshPlayList;

@end
