//
//  Palinsesto_UIViewController.m
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Palinsesto_UIViewController.h"
#import "Teleticino_iPadAppDelegate.h"
#import "Common.h"
#import "TBXML.h"

@implementation Palinsesto_UIViewController

#pragma mark -
#pragma mark View lifecycle

- (BOOL)webView:(UIWebView*)mywebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType{
    if (UIWebViewNavigationTypeLinkClicked == navigationType){
        NSURL *url=[request URL];
        NSString *scheme = [[url scheme] lowercaseString];
        if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"]){
            [[UIApplication sharedApplication] openURL:url]; 
            return NO;
        }
        else {
            return YES;
        }
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    webView.hidden = NO;
    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('my_image').height;"];
    CGRect frame = CGRectMake(0, 361 + 50 - [height intValue], 320, [height intValue]);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [webView setFrame:frame];
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 326 - frame.size.height);
    [UIView commitAnimations];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    webView.hidden = YES;
    CGRect frame = CGRectMake(0, 411, 320, 50);
    [webView setFrame:frame];
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 326);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    StatSendOperation *stats = [[StatSendOperation alloc] init];
//    [stats sendStats];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [_segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor darkGrayColor], UITextAttributeTextColor,
                                          nil] forState:UIControlStateNormal];
    }
    
    CGRect frame = _segment.frame;
    frame.size.height = 40.0;
    [_segment setFrame:frame];
	
    self.arraySettimana = [[NSMutableArray alloc] init];
    self.dataAccess = [[DataAccess alloc] init];
    
//    _segment.tintColor = UIColorFromRGB(0x131e27);
    
    [self.table setHidden:YES];
	[self.loading startAnimating];
    
    int index = [_segment selectedSegmentIndex];
    
    if (index < 0) {
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        //[gregorian setFirstWeekday:1];
        NSDateComponents *weekdayComponents = [gregorian components:(NSWeekdayCalendarUnit) fromDate:[NSDate date]];
        NSInteger todayDayNum = [weekdayComponents weekday];
        NSLog(@"%d", todayDayNum);
        
        index = todayDayNum - 2;
        [_segment setSelectedSegmentIndex:index];
    }
	
	[_dataAccess SetItemsReceivedSelector:self :@selector(ricaricaTabella:)];
	[_dataAccess GetTeleTicinoData:@"palinsesto"];
    
    [self.table setBackgroundColor:[UIColor clearColor]];
}

- (void) giornoSelezionato:(id)sender {
    
	self.arrayDati = _arraySettimana[[sender selectedSegmentIndex]];

	[self.table reloadData];
	
	//[self.table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
	[self.table scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];

}

#define ELEMENTI 3
-(void) ricaricaTabella:(NSMutableData *)data {
    
    if (!data){
        [self.table setHidden:YES];
        [self.loading stopAnimating];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Impossibile contattare il server, connettività limitata o assente. Verifica la tua connessione a Internet e riprova. Se il problema persiste attendi qualche tempo e riprova." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        return;
    }
    
    [_segment addTarget:self action:@selector(giornoSelezionato:) forControlEvents:UIControlEventValueChanged];
	
	//NSLog(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
	
	TBXML *tbxml = [TBXML tbxmlWithXMLData:data];
	[self caricaSettimana:tbxml];
	
	self.arrayDati = _arraySettimana[[_segment selectedSegmentIndex]];
	 
    
	NSLog(@"%d",[_arrayDati count]);
	
	
	[self.table setHidden:NO];
	[self.loading stopAnimating];
	[self.table reloadData];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	return [_arrayDati count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"CellPalinsesto";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
		[[NSBundle mainBundle] loadNibNamed:@"PalinsestoCell" owner:self options:nil];
		cell = _miaCell;
		self.miaCell = nil;
    }
	
	NSMutableArray *item = _arrayDati[indexPath.row];
	UILabel *titolo = (UILabel *)[cell viewWithTag:1];
	[titolo setText: item[0]];
	titolo = (UILabel *)[cell viewWithTag:2];
	[titolo setText: item[1]];
	titolo = (UILabel *)[cell viewWithTag:3];
	[titolo setText: item[2]];
	
	NSLog(@"%@",[item objectAtIndex:2]);
	
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
}

-(NSString *)giornoOfIndex:(int)index {
	switch (index) {
		case 0:
			return @"Lunedì";
			break;
		case 1:
			return @"Martedì";
			break;
		case 2:
			return @"Mercoledì";
			break;
		case 3:
			return @"Giovedì";
			break;
		case 4:
			return @"Venerdì";
			break;
		case 5:
			return @"Sabato";
			break;
		case 6:
			return @"Domenica";
			break;
		default:
			return @"";
			break;
	}
}


- (void)caricaSettimana:(TBXML *)tbxml {

	// Obtain root element
	TBXMLElement * root = tbxml.rootXMLElement->firstChild;
	
	int index = 0;
	
	// if root element is valid
	if (root) {
		// search for the first giorno element within the root element's children
		TBXMLElement * giorno = [TBXML childElementNamed:[self giornoOfIndex:index] parentElement:root];
		
		// if a giorno element was found
		while (giorno != nil) {
			
			// instantiate an author object
			NSMutableArray * arrayTrasmissioni   = [[NSMutableArray alloc] init];
			
			// search the author's child elements for a book element
			TBXMLElement * trasmissione = [TBXML childElementNamed:@"trasmissione" parentElement:giorno];
			
			// if a book element was found
			while (trasmissione != nil) {

				NSMutableArray *temp = [[NSMutableArray alloc] init];

				TBXMLElement * ora = [TBXML childElementNamed:@"ora" parentElement:trasmissione];
				if (ora != nil) {
					[temp addObject:[TBXML textForElement:ora]];
				} else {
					[temp addObject:@""];
				}

				TBXMLElement * programma = [TBXML childElementNamed:@"programma" parentElement:trasmissione];
				if (programma != nil) {
					[temp addObject:[TBXML textForElement:programma]];
				} else {
					[temp addObject:@""];
				}
				TBXMLElement * titolo = [TBXML childElementNamed:@"titolo" parentElement:trasmissione];
				if (titolo != nil) {
					[temp addObject:[TBXML textForElement:titolo]];
				} else {
					[temp addObject:@""];
				}
				// add the book object to the author's books array and release the resource
				[arrayTrasmissioni addObject:temp];
				
				// find the next sibling element named "trasmissione"
				trasmissione = [TBXML nextSiblingNamed:@"trasmissione" searchFromElement:trasmissione];
			}
			
			// add our author object to the authors array and release the resource
			[_arraySettimana addObject:arrayTrasmissioni];
			
			++index;
			// find the next sibling element named "es. Martedi"
			giorno = [TBXML nextSiblingNamed:[self giornoOfIndex:index] searchFromElement:giorno];
		}			
	}
		
}



#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

@end

