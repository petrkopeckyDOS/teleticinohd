//
//  TeleTicino_UIViewController.h
//  TeleTicino-iPad
//
//  Created by Antonio Egizio on 16/03/11.
//  Copyright 2011 Dos Informatica ed Elettronica Sagl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "VideoContainerItem_UIView.h"
#import "DataAccess.h"
#import "AppRecord.h"
#import "Common.h"
#import "NSString+StringFix.h"

#define PAD 2

@class Radio3i_UIViewController;

enum {
    UIScrollDirectionNone    = 0,
    UIScrollDirectionRight   = 1,
    UIScrollDirectionLeft    = 2,
    UIScrollDirectionUp      = 3,
    UIScrollDirectionDown    = 4,
    UIScrollDirectionCrazy   = 5
};
typedef NSUInteger UIScrollDirection;

enum {
    UIScrollingSpeedFast    = 0,
    UIScrollingSpeedNormal  = 1
};
typedef NSUInteger UIScrollingSpeed;


@interface TeleTicino_UIViewController : UIViewController <UIScrollViewDelegate> {
    int lastContentOffset;
    int maxTag;
    float preoffset;
    float pretime;
    
    int selectedButton;
}

@property (nonatomic, strong) Radio3i_UIViewController *radio;
@property (nonatomic, unsafe_unretained) BOOL isLoading;
@property (nonatomic, strong) NSString *selectedNodeName;
@property (nonatomic, strong) NSString *selected;
@property (nonatomic, strong) NSMutableArray *arrayDati;
@property (nonatomic, strong) NSMutableDictionary *imageList;
@property (nonatomic, strong) DataAccess *dataAccess;
@property (nonatomic, weak) IBOutlet UIScrollView *videoContainer;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) UIView *pulsantiera;
@property (nonatomic, strong) UIView *radioView;
@property (nonatomic, strong) UIView *newsView;
@property (nonatomic, weak) IBOutlet UIScrollView *headingTab;
@property (nonatomic, strong) NSMutableArray *headings;
@property (nonatomic, unsafe_unretained) CGFloat headingWidth;


- (void)showNewsMovieList;
- (void)loadPreview;
- (void)nonBlockingLoading:(id)sender;
- (void)showPreview;
- (NSString*)getDescForVideoUrl:(NSString*)aVideoUrl;
- (void)addHeading:(NSString*)aHeading;
- (void) reloadData;

@end
