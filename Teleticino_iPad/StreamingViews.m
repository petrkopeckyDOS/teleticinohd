//
//  StreamingViews.m
//  TeleTicino
//
//  Created by Antonio Egizio on 09/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "StreamingViews.h"


static StreamingViews *sharedInstance = nil;



@implementation StreamingViews

#pragma mark -
#pragma mark Singleton methods

+(StreamingViews *)sharedInstance {
    static dispatch_once_t pred;
    static StreamingViews *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[StreamingViews alloc] init];
    });
    return shared;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.streamingViewsList = [[NSMutableDictionary alloc] initWithCapacity:0];
    }
    return self;
}

#pragma mark -
#pragma mark Streaming Views Management

-(void) closeAllPlayingViewBut:(id)thisView{
	NSMutableArray* keys = [[NSMutableArray alloc] initWithArray:[_streamingViewsList allKeys]];
    if (thisView)
        [keys removeObject:[thisView class]];
	NSMutableArray *tmp =  [[NSMutableArray alloc] initWithArray:[_streamingViewsList objectsForKeys:keys notFoundMarker:@"404"]];
	if (tmp){
		for (StreamingViewControllerCommon* vc in tmp)
			[vc closeStream];
		[tmp release];
	}
	[_streamingViewsList removeObjectsForKeys:keys];
    [keys removeAllObjects];
    [keys release];
}

-(void) closeStoppedStreamingViews {
	if (_streamingViewsList){
		for (id aViewClass in _streamingViewsList){
			StreamingViewControllerCommon* aView = _streamingViewsList[aViewClass];
			if(aView.isMusicStopped){
				[aView closeStream];
				[_streamingViewsList removeObjectForKey:aViewClass];
			}
		}
	}
}

-(id) getObjectForClass:(id)streamingViewControllerClass{
	return _streamingViewsList[streamingViewControllerClass];
}

-(void)addStreamingView:(id)streamingView ofClass:(id)streamingViewClass{
	_streamingViewsList[streamingViewClass] = streamingView;
}

@end
