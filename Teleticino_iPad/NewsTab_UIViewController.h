//
//  Ticinonews_UIViewController.h
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "NewsList_UIViewController.h"
#import "Detail_UIViewController.h"

@protocol NewsTabDelegate;

@interface NewsTab_UIViewController : NewsList_UIViewController <IconDownloaderDelegate, UIScrollViewDelegate, DetailViewDelegate> {
    
}

@property (nonatomic, strong) UIImageView *overflowRight;
@property (nonatomic, strong) UIImageView *overflowLeft;
@property (nonatomic, unsafe_unretained) MPMoviePlayerViewController* moviePlayerView;
@property (nonatomic, unsafe_unretained) id delegate;
@property (nonatomic, weak) IBOutlet UILabel *loadingLabel;
@property (nonatomic, strong) Detail_UIViewController *detailView;

@end