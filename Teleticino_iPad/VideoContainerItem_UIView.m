//
//  VideoContainerItem_UIView.m
//  TeleTicino-iPad
//
//  Created by Antonio Egizio on 16/03/11.
//  Copyright 2011 Dos Informatica ed Elettronica Sagl. All rights reserved.
//

#import "VideoContainerItem_UIView.h"

#define degreesToRadians(x) (M_PI * x / 180.0)

@interface VideoContainerItem_UIView (Private)

- (CATransform3D)getTransformForFlowState:(UIFlowState)aState;
- (UIImage *)reflectedImageRepresentationWithHeight:(NSUInteger)height;

@end

static CGImageRef AEViewCreateGradientImage (int pixelsWide, int pixelsHigh){
	CGImageRef theCGImage = NULL;
    CGContextRef gradientBitmapContext = NULL;
    CGColorSpaceRef colorSpace;
	CGGradientRef grayScaleGradient;
	CGPoint gradientStartPoint, gradientEndPoint;
    
    colorSpace = CGColorSpaceCreateDeviceGray();
    gradientBitmapContext = CGBitmapContextCreate (NULL, pixelsWide, pixelsHigh,
												   8, 0, colorSpace, kCGImageAlphaNone);
	
	if (gradientBitmapContext != NULL) {
		CGFloat colors[] = {0.0, 1.0,1.0, 1.0,};
		grayScaleGradient = CGGradientCreateWithColorComponents(colorSpace, colors, NULL, 2);
		gradientStartPoint = CGPointZero;
		gradientEndPoint = CGPointMake(0,pixelsHigh);
		CGContextDrawLinearGradient (gradientBitmapContext, grayScaleGradient, gradientStartPoint, gradientEndPoint, kCGGradientDrawsAfterEndLocation);
		CGGradientRelease(grayScaleGradient);
		theCGImage=CGBitmapContextCreateImage(gradientBitmapContext);
		CGContextRelease(gradientBitmapContext);
	}
	CGColorSpaceRelease(colorSpace);
    return theCGImage;
}

@implementation VideoContainerItem_UIView

@synthesize thumbnail, showReflect, detail, flowState, utilityButton;

- (id)initWithFrame:(CGRect)frame{
    frame = CGRectMake(frame.origin.x, frame.origin.y, kWidth, kHeight + (kHeight * reflectionFraction)); //225 Height of thumbnail image + 75 Height of reflection Image
    self = [super initWithFrame:frame];
    if (self) {
        CGRect imgFrame = CGRectMake(0, 0, kWidth, kHeight);
        thumbnail = [[UIImageView alloc] initWithFrame:imgFrame];
        thumbnail.image = [UIImage imageNamed:@"preview.jpg"];
        imgFrame = CGRectMake(0, kHeight, kWidth, kHeight * reflectionFraction);
        [self addSubview:thumbnail];
        CGRect reflectionRect=self.thumbnail.frame;
        reflectionRect.size.height=reflectionRect.size.height*reflectionFraction;
        reflectionRect=CGRectOffset(reflectionRect,0,self.thumbnail.frame.size.height);
        self.localReflectionImageView = [[UIImageView alloc] initWithFrame:reflectionRect];
        NSUInteger reflectionHeight=self.thumbnail.bounds.size.height*reflectionFraction;
        _localReflectionImageView.image=[self reflectedImageRepresentationWithHeight:reflectionHeight];
        _localReflectionImageView.alpha=reflectionOpacity;
        [self addSubview:_localReflectionImageView];
        showReflect = YES;
        flowState = YES;
    }
    return self;
}

- (id)init{
    CGRect frame = CGRectMake(0, 0, kWidth, kHeight + (kHeight * reflectionFraction)); //225 Height of thumbnail image + 75 Height of reflection Image
    if ((self = [super initWithFrame:frame])){
        //Initialization already done in the initWithFrame Method
    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    frame = CGRectMake(frame.origin.x, frame.origin.y, kWidth, kHeight + (kHeight * reflectionFraction)); //225 Height of thumbnail image + 75 Height of reflection Image
    [super setFrame:frame];
}

- (void)setThumbnailImage:(UIImage*)anImage{
    UIImage *image = [anImage copy];
    NSLog(@"%f",image.size.width);
    NSLog(@"%f",image.size.height);
    double imageHeight = kWidth * image.size.height / image.size.width;
    CGSize itemSize = CGSizeMake(kWidth, imageHeight);
    UIGraphicsBeginImageContext(itemSize);
        
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width + 60, itemSize.height);
    [image drawInRect:imageRect];
    thumbnail.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //thumbnail.image = [anImage ];
    CGRect reflectionRect=self.thumbnail.frame;
    reflectionRect.size.height=reflectionRect.size.height*reflectionFraction;
    reflectionRect=CGRectOffset(reflectionRect,0,self.thumbnail.frame.size.height);
    NSUInteger reflectionHeight=self.thumbnail.bounds.size.height*reflectionFraction;
    _localReflectionImageView.image=[self reflectedImageRepresentationWithHeight:reflectionHeight];
}

- (void)setThumbnail:(UIImageView *)aThumbnail{
    [self setThumbnailImage:thumbnail.image];
}

- (void)xcenter{
    flowState = UIFlowStateCenter;
    _localReflectionImageView.alpha=reflectionOpacity;
    [self.layer setZPosition:200];
    self.layer.transform = [self getTransformForFlowState:UIFlowStateCenter];
    if (utilityButton)
        self.utilityButton.hidden = NO;
}


- (void)xleft{
    flowState = UIFlowStateLeft;
    _localReflectionImageView.alpha=reflectionOpacity / 2;
    [self.layer setZPosition:150];
    if (utilityButton)
        self.utilityButton.hidden = YES;
    self.layer.transform = [self getTransformForFlowState:UIFlowStateLeft];
    [self.layer setZPosition:-50];
}

- (void)xright{
    flowState = UIFlowStateRight;
    _localReflectionImageView.alpha=reflectionOpacity / 2;
    [self.layer setZPosition:150];
    if (utilityButton)
        self.utilityButton.hidden = YES;
    self.layer.transform = [self getTransformForFlowState:UIFlowStateRight];
    [self.layer setZPosition:-50];
}

- (CATransform3D)getTransformForFlowState:(UIFlowState)aState{
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = -4.0f / 700.0f;
    
    float degrees = 0.0f;
    switch (aState) {
        case UIFlowStateCenter:
            degrees = 0.0f;
            break;
        case UIFlowStateRight:
            degrees = -kInclination;
            transform = CATransform3DScale(transform, kScale, kScale, kScale / 2 );
            break;
        case UIFlowStateLeft:
            degrees = kInclination;
            transform = CATransform3DScale(transform, kScale, kScale, kScale / 2);
            break;
        default:
            degrees = 90.0f;
            break;
    }
    
    transform = CATransform3DRotate(transform, degreesToRadians(degrees), 0.0f, 1.0f, 0.0f);
    return transform;
}

- (UIImage *)reflectedImageRepresentationWithHeight:(NSUInteger)height{
    
	CGContextRef mainViewContentContext;
    CGColorSpaceRef colorSpace;
	
    colorSpace = CGColorSpaceCreateDeviceRGB();
    mainViewContentContext = CGBitmapContextCreate (NULL, thumbnail.bounds.size.width,height, 8,0, colorSpace, kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorSpace);	
	
	if (mainViewContentContext==NULL)
		return NULL;
	
	CGFloat translateVertical=thumbnail.bounds.size.height-height;
	CGContextTranslateCTM(mainViewContentContext,0,-translateVertical);
	[self.layer renderInContext:mainViewContentContext];
	CGContextTranslateCTM(mainViewContentContext,0,translateVertical);
	CGImageRef mainViewContentBitmapContext=CGBitmapContextCreateImage(mainViewContentContext);
	CGContextRelease(mainViewContentContext);
	CGImageRef gradientMaskImage=AEViewCreateGradientImage(1,height);
	CGImageRef reflectionImage=CGImageCreateWithMask(mainViewContentBitmapContext,gradientMaskImage);
	CGImageRelease(mainViewContentBitmapContext);
	CGImageRelease(gradientMaskImage);
	UIImage *theImage=[UIImage imageWithCGImage:reflectionImage];
	CGImageRelease(reflectionImage);
	return theImage;
}

#define kButtonDimension 72

-(void)setUtilityButton:(UIButton *)aButton{
    utilityButton = aButton;
    utilityButton.frame = CGRectMake((thumbnail.frame.origin.x + thumbnail.frame.size.width - kButtonDimension) / 2, (thumbnail.frame.origin.y + thumbnail.frame.size.height - kButtonDimension) / 2, kButtonDimension, kButtonDimension);
    utilityButton.hidden = YES;
    utilityButton.tag = self.tag;
    [self addSubview:utilityButton];
}


@end
