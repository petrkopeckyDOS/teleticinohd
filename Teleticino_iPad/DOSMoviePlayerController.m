//
//  DOSMoviePlayerController.m
//  Teleticino_iPad
//
//  Created by Davide on 04.02.14.
//
//

#import "DOSMoviePlayerController.h"

@implementation DOSMoviePlayerController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return _orientationMask;
}

@end
