//
//  AWRegularLabel.m
//  1trip2 iOS
//
//  Created by Davide Vincenzi on 13.01.14.
//  Copyright (c) 2014 AugmentedWorks. All rights reserved.
//

#import "AWLabel.h"

@implementation AWRegularLabel

- (void)awakeFromNib {
    [super awakeFromNib];
    
    float fontSize = self.font.pointSize;
    [self setFont:[UIFont fontWithName:@"Replica-Regular" size:fontSize]];
}

@end


@implementation AWBoldLabel

- (void)awakeFromNib {
    [super awakeFromNib];
    
    float fontSize = self.font.pointSize;
    [self setFont:[UIFont fontWithName:@"Replica-Bold" size:fontSize]];
}

@end



@implementation AWRegularButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    float fontSize = self.titleLabel.font.pointSize;
    [self.titleLabel setFont:[UIFont fontWithName:@"Replica-Regular" size:fontSize]];
}

@end


@implementation AWBoldButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    float fontSize = self.titleLabel.font.pointSize;
    [self.titleLabel setFont:[UIFont fontWithName:@"Replica-Bold" size:fontSize]];
}

@end
