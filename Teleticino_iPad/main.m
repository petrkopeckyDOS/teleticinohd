//
//  main.m
//  Teleticino_iPad
//
//  Created by Davide Vincenzi on 20.04.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Teleticino_iPadAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([Teleticino_iPadAppDelegate class]));
    }
}