//
//  Radio3i_UIViewController.m
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Radio3i_UIViewController.h"
#import "Common.h"
#import "AppRecord.h"
#import "Info_UIViewController.h"
#import "Teleticino_iPadAppDelegate.h"

@implementation Radio3i_UIViewController

@synthesize podcast;
@synthesize buttonRadio,buttonTg;
@synthesize radioState, tgState;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    StatSendOperation *stats = [[StatSendOperation alloc] init];
//    [stats sendStats];
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopAudio) name:@"stopAudio" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playAudio) name:@"playAudio" object:nil];
    
    UIButton* infoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setLeftBarButtonItem:modalButton animated:YES];
     */
    /*
    CGRect frame = CGRectMake(20, 250, 232, 9);
    
	self.volumeView = [[MPVolumeView alloc] initWithFrame:frame];
    [_volumeView sizeToFit];
    [_volumeView setRouteButtonImage:[UIImage imageNamed:@"airplay"] forState:UIControlStateNormal];
    [self.view addSubview:_volumeView];
    */
    //self.radioState = @"stop";
    //self.tgState = @"stop";
}

- (void) stopAudio
{
    [self setStopped];
    [streamer stop];
}

- (void) playAudio {
    [self setPlaying];
    [streamer start];
}

- (IBAction)goToStore:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/ch/app/radio3i/id1090017251?l=it&mt=8"]];
}


- (void)infoButtonAction:(id)sender{
    Info_UIViewController* info =[[Info_UIViewController alloc] initWithNibName:@"Info_UIViewController" bundle:nil];
    info.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.tabBarController presentModalViewController:info animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	[[StreamingViews sharedInstance] closeStoppedStreamingViews];
}

#pragma mark gestione eventi 
- (IBAction) contatta:(UIButton*)sender {
	if (!_contatta) {
        self.contatta = [[Contatta_UIViewController alloc] initWithNibName:@"Contatta_UIViewController" bundle:nil];
    }
    if ([_popoverCont isPopoverVisible]){
        [_popoverCont dismissPopoverAnimated:YES];
    }
    self.popoverCont = [[UIPopoverController alloc] initWithContentViewController:_contatta];
    _popoverCont.popoverContentSize = CGSizeMake(320, 400);
    [_popoverCont presentPopoverFromRect:CGRectMake(sender.frame.origin.x, sender.frame.origin.y + sender.frame.size.height /2, sender.frame.size.width, sender.frame.size.height) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    
}

- (IBAction) playlist:(UIButton *)sender{
    if (!_playlist)
        self.playlist = [[Playlist_UIViewController alloc] initWithNibName:@"Playlist_UIViewController" bundle:nil];
	if ([_popoverCont isPopoverVisible]){
        [_popoverCont dismissPopoverAnimated:YES];
    }
    self.popoverCont = [[UIPopoverController alloc] initWithContentViewController:_playlist];
    _popoverCont.popoverContentSize = CGSizeMake(320, 450);
    [_popoverCont presentPopoverFromRect:CGRectMake(sender.frame.origin.x, sender.frame.origin.y + sender.frame.size.height /2, sender.frame.size.width, sender.frame.size.height) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];

}

- (IBAction) podcast:(UIButton *)sender {
    if (podcast) { //Always reload table!
        self.podcast = nil;
    }
    
    self.podcast = [[Podcast_UIViewController alloc] initWithNibName:@"Podcast_UIViewController" bundle:nil];
    [self.podcast setDelegate:self];
    self.popoverCont = [[UIPopoverController alloc] initWithContentViewController:podcast];
    
    _popoverCont.popoverContentSize = CGSizeMake(320, 450);
    [_popoverCont presentPopoverFromRect:CGRectMake(sender.frame.origin.x, sender.frame.origin.y + sender.frame.size.height /2, sender.frame.size.width, sender.frame.size.height) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    
    
}

//#define liveURL @"http://scfire-ntc-aa08.stream.aol.com:80/stream/1048"

//#define liveURL @"http://213.251.171.73:8081/"
//#define liveURL @"http://radio3i-1.streaming.init7.net:80/mobile"
//#define rgUrl @"http://www.ticinonews.ch/multimedia/audio/rg.mp3"

#define liveURL @"http://radio3i.ice.infomaniak.ch/radio3i-64.mp3"
#define rgUrl @"http://www.ticinonews.ch/multimedia/audio/rg.mp3"

- (IBAction) ascolta_la_diretta:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopVideo" object:nil];
    
	[self startStreaming:sender];
}

//
// radio_giornale:
//
// Handles the play/stop button. Creates, observes and starts the
// audio streamer when it is a play button. Stops the audio streamer when
// it isn't.
//
// Parameters:
//    sender - normally, the play/stop button.
//
- (void) startStreaming:(UIButton *)button {

	[[StreamingViews sharedInstance] addStreamingView:self ofClass:[self class]];
    
    if (button == buttonRadio ) {
        
        if ([self.radioState isEqualToString:@"play"]) {
            
            [self stopAudio];
            
        } else {
            
            self.radioState = @"play";
            self.tgState = @"stop";
            
            [self spinButton:button];
            
            [[StreamingViews sharedInstance] closeAllPlayingViewBut:self];
            
            [self createStreamer:liveURL];
            
        }
        
    }
    
    if (button == buttonTg ) {
        
        if ([self.tgState isEqualToString:@"play"]) {
            
            [self stopAudio];
            
        } else {
            
            self.radioState = @"stop";
            self.tgState = @"play";
            
            [self spinButton:button];
            
            [[StreamingViews sharedInstance] closeAllPlayingViewBut:self];
            
            [self createStreamer:rgUrl];
            
            
        }
    }
}


//
// spinButton
//
// Shows the spin button when the audio is loading. This is largely irrelevant
// now that the audio is loaded from a local file.
//
- (void)spinButton:(UIButton *)button {
    [button setImage:[UIImage imageNamed:@"radio3i_icona_wait"] forState:UIControlStateNormal];
    
	[CATransaction begin];
	[CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
	CGRect frame = [button frame];
	button.layer.anchorPoint = CGPointMake(0.5, 0.5);
	button.layer.position = CGPointMake(frame.origin.x + 0.5 * frame.size.width, frame.origin.y + 0.5 * frame.size.height);
	[CATransaction commit];
	[CATransaction begin];
	[CATransaction setValue:(id)kCFBooleanFalse forKey:kCATransactionDisableActions];
	[CATransaction setValue:@2.0f forKey:kCATransactionAnimationDuration];
	CABasicAnimation *animation;
	animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
	animation.fromValue = @0.0f;
	animation.toValue = [NSNumber numberWithFloat:2 * M_PI];
	animation.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionLinear];
	animation.delegate = self;
	[button.imageView.layer addAnimation:animation forKey:@"rotationAnimation"];
	[CATransaction commit];
}
- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)finished {
    
	if (finished) {
        if ([self.radioState isEqualToString:@"play"])
            [self spinButton:self.buttonRadio];
        if ([self.tgState isEqualToString:@"play"])
            [self spinButton:self.buttonTg];
	}
    
}


- (void)setStopped {
    
    [_progressSlider setValue:0.0];
	[_progressSlider setEnabled:NO];
 	CGRect currentFrame = _volumeSlider.frame;
	CGRect frameSlide = CGRectMake(-320, currentFrame.origin.y, currentFrame.size.width, currentFrame.size.height);
	[UIView beginAnimations:nil context:nil];
	[_volumeSlider setFrame:frameSlide];
	[UIView setAnimationDuration:0.5];
	[UIView commitAnimations];
}

- (void)setPlaying {
    
	CGRect frame = CGRectMake(20, 10, 280, 30);
    CGRect currentFrame = _volumeSlider.frame;
	CGRect frameSlide = CGRectMake(0, currentFrame.origin.y, currentFrame.size.width, currentFrame.size.height);
	if (_volumeView == nil) {
		self.volumeView = [[MPVolumeView alloc] initWithFrame:frame];
        [_volumeSlider setHidden:NO];
		[_volumeView sizeToFit];
		[_volumeSlider addSubview:_volumeView];
	}
	[UIView beginAnimations:nil context:nil];
	[_volumeSlider setFrame:frameSlide];
	[UIView setAnimationDuration:0.5];
	[UIView commitAnimations];
    
}


- (IBAction)sliderMoved:(UISlider *) aSlider {
	
	if (streamer.duration)
	{	
		self.seeking = YES;
		self.progress = streamer.progress;
		self.duration = streamer.duration;

		self.timerIsCountingDown = NO;
		self.newSeekTime = (_progressSlider.value / 100.0) * _duration;
		self.progress = _newSeekTime;
		int min = floor((_progress / 60));
		int s = ((int)_progress % 60);
		int remMin = floor(_duration / 60) - min;
		int remS = ((int)(_duration-(min*60+s))%60);
		[_playedLabel setText:[NSString stringWithFormat:@"%02d:%02d",min,s]];
		[_remainingLabel setText:[NSString stringWithFormat:@"- %02d:%02d",remMin,remS]];
	}
}


- (IBAction)sliderTouchUpInside:(UISlider *)aSlider{
	self.timerIsCountingDown = YES;
	if(_newSeekTime >= (_duration-1)){
		[streamer stop];
	}else
        [streamer seekToTime:_newSeekTime];
}

- (void)updateProgress:(NSTimer *)updatedTimer{
	
	if(self.timerIsCountingDown){
		if (streamer.bitRate != 0.0){
			self.progress = streamer.progress;
			self.duration = streamer.duration;
			if (_duration > 0){
				int min = floor((_progress / 60));
				int s = ((int)_progress % 60);
				int remMin = floor(_duration / 60) - min;
				int remS = ((int)(_duration-(min*60+s))%60);
				[_playedLabel setText:[NSString stringWithFormat:@"%02d:%02d",min,s]];
				[_remainingLabel setText:[NSString stringWithFormat:@"- %02d:%02d",remMin,remS]];
				[_progressSlider setEnabled:YES];
				if (!_seeking)
					[_progressSlider setValue:100 * _progress / _duration];
			}
			else{
				[_progressSlider setEnabled:NO];
			}
		}
		else{
			_playedLabel.text = @"00:00";
			_remainingLabel.text = @"- 00:00";
		}
	}
}

//
// playbackStateChanged:
//
// Invoked when the AudioStreamer
// reports that its playback status has changed.
//
- (void)playbackStateChanged:(NSNotification *)aNotification {
	
	if ([streamer isWaiting]) {
        NSLog(@"waiting");
        
	} else if ([streamer isPlaying]) {
        
        [self setPlaying];
        
        NSLog(@"playing");
        [self.buttonRadio.imageView.layer removeAllAnimations];
        
        if ([self.radioState isEqualToString:@"play"]) {
            
            [self.buttonRadio setImage:[UIImage imageNamed:@"radio3i_icona_stop"] forState:UIControlStateNormal];
            
        } else {
            
            [self.buttonRadio setImage:[UIImage imageNamed:@"radio3i_icona_onair"] forState:UIControlStateNormal];
        }
        
        [self.buttonTg.imageView.layer removeAllAnimations];
        
        if ([self.tgState isEqualToString:@"play"]) {
            
            [self.buttonTg setImage:[UIImage imageNamed:@"radio3i_icona_stop"] forState:UIControlStateNormal];
            [self showProgressSlider];
            
        } else {
            
            [self.buttonTg setImage:[UIImage imageNamed:@"radio3i_icona_tg"] forState:UIControlStateNormal];
            [self hideProgressSlider];
            
        }
        
	}
	else if ([streamer isPaused]) {
        
        NSLog(@"paused");
        
        [self setStopped];
        [self.buttonRadio setImage:[UIImage imageNamed:@"radio3i_icona_onair"] forState:UIControlStateNormal];
        [self.buttonTg setImage:[UIImage imageNamed:@"radio3i_icona_tg"] forState:UIControlStateNormal];
        
        self.tgState = @"stop";
        self.radioState = @"stop";
        
        [self hideProgressSlider];
        
        [_progressUpdateTimer invalidate];
		self.progressUpdateTimer = nil;
        
	}
    
	else if ([streamer isIdle]) {
        
        NSLog(@"idle");
        
        if (_waitingForStop == YES) {
            
            [streamer start];
            self.waitingForStop = NO;
            
        } else {
            
            self.tgState = @"stop";
            self.radioState = @"stop";
            [self setStopped];
            [self.buttonRadio setImage:[UIImage imageNamed:@"radio3i_icona_onair"] forState:UIControlStateNormal];
            [self.buttonTg setImage:[UIImage imageNamed:@"radio3i_icona_tg"] forState:UIControlStateNormal];
            
            [self hideProgressSlider];
            
            [_progressUpdateTimer invalidate];
            self.progressUpdateTimer = nil;
            
        }
	}
}




//
// setButtonImage:
//
// Used to change the image on the playbutton. This method exists for
// the purpose of inter-thread invocation because
// the observeValueForKeyPath:ofObject:change:context: method is invoked
// from secondary threads and UI updates are only permitted on the main thread.
//
// Parameters:
//    image - the image to set on the play button.
//

- (UIButton *)activeButton {
	return buttonRadio;
}

-(void) showProgressSlider{
    
    CGRect frameSlide = CGRectMake(20, _progressView.frame.origin.y, _progressView.frame.size.width, _progressView.frame.size.height);
	[UIView beginAnimations:nil context:nil];
	[_progressView setFrame:frameSlide];
	[UIView setAnimationDuration:0.5];
	[UIView commitAnimations];
}

-(void) hideProgressSlider{
    CGRect frameSlide = CGRectMake(-285, _progressView.frame.origin.y, _progressView.frame.size.width, _progressView.frame.size.height);
	[UIView beginAnimations:nil context:nil];
	[_progressView setFrame:frameSlide];
	[UIView setAnimationDuration:0.5];
	[UIView commitAnimations];
}


- (void)setButtonImage:(UIImage *)image forState:(int)controlState {
	
	[[self.view viewWithTag:1].layer removeAllAnimations];
    [[self.view viewWithTag:3].layer removeAllAnimations];
	if (!image) {
        if ([self activeButton].tag == 1)
            [[self activeButton] setImage:[UIImage imageNamed:@"Play"] forState:controlState];
        else if ([self activeButton].tag == 1)
            [[self activeButton] setImage:[UIImage imageNamed:@"World-dis"] forState:controlState];
	} else {
		[[self activeButton] setImage:image forState:controlState];
		
		if ([[self activeButton].currentImage isEqual:[UIImage imageNamed:@"Loading"]]){
			[self spinButton:[self activeButton]];
		}
	}
	
}

//
// destroyStreamer
//
// Removes the streamer, the UI update timer and the change notification
//
- (void)destroyStreamer {
	
	if (streamer) {
		[[NSNotificationCenter defaultCenter]
		 removeObserver:self
		 name:ASStatusChangedNotification
		 object:streamer];
        [_progressUpdateTimer invalidate];
		_progressUpdateTimer = nil;
		
		[streamer stop];
		streamer = nil;
	}
}


//
// createStreamer
//
// Creates or recreates the AudioStreamer object.
//


- (void) createStreamer:(NSString *)url {
    
	//NSString *streamUrl = [[NSString alloc] initWithString:url];
	//NSString *escapedValue = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,(CFStringRef)streamUrl,NULL,NULL,kCFStringEncodingUTF8));
	NSURL *myurl = [NSURL URLWithString:url];
    
    self.seeking = NO;
    [_progressSlider setValue:0];
    
	if (streamer) {
        if ([streamer isPlaying]) {
            if ([self.radioState isEqualToString:@"play"] || [self.tgState isEqualToString:@"play"]) {
                self.waitingForStop = YES;
            }
            [streamer stop];
        } else {
            [streamer start];
        }
        [streamer changeURL:myurl];
        
    } else {
        self.waitingForStop = NO;
        streamer = [[AudioStream alloc] initWithURL:myurl];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackStateChanged:) name:ASStatusChangedNotification object:streamer];
        [streamer start];
    }
    //if ([self.tgState isEqualToString:@"play"])
        //self.progressUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateProgress:) userInfo:nil repeats:YES];
    
}

- (void)podcastViewControllerShouldDismiss:(Podcast_UIViewController *)podcastVC {
    [self.popoverCont dismissPopoverAnimated:NO];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    //[self destroyStreamer];
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)dealloc {
    /*
     [[NSNotificationCenter defaultCenter] removeObserver:self];
     [UIView commitAnimations]; //End all animations
     
     if (_progressUpdateTimer){
     [_progressUpdateTimer invalidate];
     self.progressUpdateTimer = nil;
     }
     
     [self destroyStreamer];
     */
}


@end

