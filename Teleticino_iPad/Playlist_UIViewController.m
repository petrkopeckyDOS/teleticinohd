//
//  Playlist_UIViewController.m
//  TeleTicino
//
//  Created by Antonio Egizio on 09/03/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "Playlist_UIViewController.h"


@implementation Playlist_UIViewController

@synthesize table;
@synthesize miaCell;
@synthesize dataAccess;
@synthesize loading;
@synthesize arrayDati;

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self selector:@selector(refreshPlayList) userInfo: nil repeats:YES];
	[self refreshPlayList];
}

- (BOOL)webView:(UIWebView*)mywebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType{
    if (UIWebViewNavigationTypeLinkClicked == navigationType){
        NSURL *url=[request URL];
        NSString *scheme = [[url scheme] lowercaseString];
        if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"]){
            [[UIApplication sharedApplication] openURL:url]; 
            return NO;
        }
        else {
            return YES;
        }
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    webView.hidden = NO;
    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('my_image').height;"];
    CGRect frame = CGRectMake(0, 361 + 50 - [height intValue], 320, [height intValue]);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [webView setFrame:frame];
    //self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 338 - frame.size.height);
    [UIView commitAnimations];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    webView.hidden = YES;
    //self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 317);
    CGRect frame = CGRectMake(0, 411, 320, 50);
    [webView setFrame:frame];
}

#pragma mark - View lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    
    StatSendOperation *stats = [[StatSendOperation alloc] init];
    [stats sendStats];
    
  	self.navigationItem.title = @"Playlist";
    arrayDati = [[NSMutableArray alloc] initWithCapacity:0];
    dataAccess = [[DataAccess alloc] init];
    isDOWNLOADING = NO;
    
    [self.table setBackgroundColor:[UIColor clearColor]];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [timer invalidate];
	timer = nil;
}

#pragma mark -
#pragma mark Tableview refreshing

- (void)refreshPlayList {
	if (isDOWNLOADING) return;
	
	isDOWNLOADING = YES;
	
	[self.table setHidden:YES];
	[self.loading startAnimating];
	[dataAccess SetItemsReceivedSelector:self :@selector(ricaricaTabella:)];
	[dataAccess GetTeleTicinoData:@"playlist"];
}

#pragma mark -
#pragma mark Table View Life Cycle

#define ELEMENTI 7
-(void) ricaricaTabella:(NSMutableData *)data {
    
    if (!data){
        [self.table setHidden:YES];
        [self.loading stopAnimating];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Impossibile contattare il server, connettività limitata o assente. Verifica la tua connessione a Internet e riprova. Se il problema persiste attendi qualche tempo e riprova." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        isDOWNLOADING = NO;
        return;
    }
	
	NSLog(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
	
	TBXML *tbxml = [TBXML tbxmlWithXMLData:data];
	NSMutableArray *temp = [Common XMLGetItemsBelowNodeName:tbxml.rootXMLElement NodeName:@"playlist"];
	
	AppRecord *appRecord = nil;
	int j = 0;
	int x = 0;
	
	for (int i = 0; i < [temp count]; i++) {
		if (j == 0)	{
			++x;
			if ([arrayDati count] < x) {
				appRecord = [[AppRecord alloc] init];	
			} else {
				appRecord = arrayDati[(x - 1)];
			}
		}
		if (j == 0) {
			appRecord.pubDate = ((ItemKeyValue *)temp[i]).nodeValue;
		}	
		if (j == 1) {
			appRecord.title = ((ItemKeyValue *)temp[i]).nodeValue;
			appRecord.title = [appRecord.title stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
		}	
		
		++j;
		if (j == ELEMENTI)	{
			if ([arrayDati count] < x) {
				[arrayDati addObject:appRecord];
			} 
			j = 0;
			
		}
	}
	
	NSLog(@"%d",[arrayDati count]);
	
	[self.table setHidden:NO];
	[self.loading stopAnimating];
	[self.table reloadData];
	[self.table scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
	isDOWNLOADING = NO;
	
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [arrayDati count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"SongCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
		[[NSBundle mainBundle] loadNibNamed:@"SongCell" owner:self options:nil];
		cell = miaCell;
		self.miaCell = nil;
    }
	
	int nodeCount = [self.arrayDati count];
	// Leave cells empty if there's no data yet
    if (nodeCount > 0)
	{
        // Set up the cell...
        AppRecord *appRecord = (self.arrayDati)[indexPath.row];
		
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm:ss"];
		NSDate *pubDate = [dateFormatter dateFromString: appRecord.pubDate];

        [cell setBackgroundColor:[UIColor clearColor]];
        
        UILabel *ora = (UILabel *)[cell viewWithTag:1];
        [ora setTextColor:UIColorFromRGB(0x5f5f5f)];
        
		UILabel *titolo = (UILabel *)[cell viewWithTag:2];
		[titolo setText: appRecord.title];
        [titolo setTextColor:UIColorFromRGB(0x5f5f5f)];

        [dateFormatter setDateFormat:@"HH:mm"];
        [ora setText: [NSString stringWithFormat: @"%@",[dateFormatter stringFromDate:pubDate]]];
        
		if (indexPath.row == 0) {
			
			NSDate *now = [[NSDate alloc] init];
			int secondi = [now timeIntervalSinceDate: pubDate];
			if (secondi > 0 && secondi < 240) {
                [cell.contentView setBackgroundColor:UIColorFromRGB(0xF49C1F)];
                [ora setTextColor:UIColorFromRGB(0xffffff)];
                [titolo setTextColor:UIColorFromRGB(0xffffff)];
			}
		}
        
        
    }
    return cell;
}

#pragma mark -
#pragma mark Memory management methods

- (void)dealloc{
    if (timer) {
		[timer invalidate];
		timer = nil;
	}
    [arrayDati removeAllObjects];

}


@end
