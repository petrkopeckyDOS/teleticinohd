//
//  NSString+StringFix.m
//  TeleTicino
//
//  Created by Antonio Egizio on 18/02/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import "NSString+StringFix.h"

static NSDictionary *htmlEscapes = nil;
static NSDictionary *htmlUnescapes = nil;
static NSString *replaceAll(NSString *s, NSDictionary *replacements) {
	for (NSString *key in replacements) {
		NSString *replacement = replacements[key];
		s = [s stringByReplacingOccurrencesOfString:key withString:replacement];
	}
	return s;
}

@implementation NSString (StringFix)


-(NSString*)stringByRemovingAccents {
    return [[NSString alloc] initWithData:[self
											dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]
								  encoding:NSASCIIStringEncoding];
}

-(NSString *)stringByFixingNonValidCharacters{
	
	NSString *trimSpace = [[self componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]] componentsJoinedByString:@""];
	
	return [NSString stringWithString:[[trimSpace stringByRemovingAccents] lowercaseString]];
}

+ (NSDictionary *)htmlEscapes {
	if (!htmlEscapes) {
		htmlEscapes = @{@"&": @"&amp;",
					   @"<": @"&lt;",
					   @">": @"&gt;"};
	}
	return htmlEscapes;
}

+ (NSDictionary *)htmlUnescapes {
	if (!htmlUnescapes) {
		htmlUnescapes = @{@"&amp;": @"&",
						 @"&lt;": @"<", 
						 @"&gt;": @">"};
	}
	return htmlEscapes;
}

- (NSString *)htmlEscapedString {
	return replaceAll(self, [[self class] htmlEscapes]);
}

- (NSString *)htmlUnescapedString {
	return replaceAll(self, [[self class] htmlUnescapes]);
}

@end