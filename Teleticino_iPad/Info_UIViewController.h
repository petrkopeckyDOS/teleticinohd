//
//  Info_UIViewController.h
//  TeleTicino
//
//  Created by Antonio Egizio on 24/03/11.
//  Copyright 2011 DOS Informatica e elettronica sagl. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Info_UIViewController : UIViewController <UIWebViewDelegate>{
    
}

@property (nonatomic, unsafe_unretained) BOOL firstLoad;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, weak) IBOutlet UIWebView *infoWeb;

- (IBAction)exitPressed:(id)sender;

@end
