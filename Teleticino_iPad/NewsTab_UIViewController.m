//
//  Ticinonews_UIViewController.m
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewsTab_UIViewController.h"
#import "StreamingViews.h"
#import "Teleticino_iPadAppDelegate.h"
#import "Detail_UIViewController.h"

#define _MAINVIEW [self.view viewWithTag:365]

@implementation NewsTab_UIViewController

#pragma mark -
#pragma mark View lifecycle

- (void) updateArrows {
    if (self.headingTab.contentOffset.x < (self.headingTab.contentSize.width - 339)) {
        
        [[self.view viewWithTag:332] setAlpha:1.0];
        
    } else {
        [[self.view viewWithTag:332] setAlpha:0.1];
        
    }
    if (self.headingTab.contentOffset.x > 0) {
        
        [[self.view viewWithTag:331] setAlpha:1.0];
        
    } else {
        [[self.view viewWithTag:331] setAlpha:0.1];
    }
}

- (void) animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [_detailView.view removeFromSuperview];
    self.detailView = nil;
}

- (void) presentDetail:(Detail_UIViewController *)detail
{
    [detail setModalTransitionStyle:UIModalTransitionStylePartialCurl];
    [self presentViewController:detail animated:YES completion:^{
        [detail.view.superview addSubview:self.view];
    }];
}

- (void) dismissAndRepresent
{
    UIViewController *controller = self.modalViewController;
    if (controller)
    {
        [self dismissModalViewControllerAnimated:NO];
        [self presentModalViewController:controller animated:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

	[self loadTable];
	
	NSString *rubrica = [[NSString alloc] initWithFormat:@"%@",((UIButton*)(self.headings)[selected]).currentTitle];
	[self loadTable];
	
	[self.dataAccess GetTeleTicinoData:rubrica];
    
    [self updateArrows];
    
    CGRect frame2 = [self.view viewWithTag:698].frame;
    [[self.view viewWithTag:698] setFrame:CGRectMake(frame2.origin.x, frame2.origin.y - 100, frame2.size.width, frame2.size.height)];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ricaricaTutto) name:@"ricaricaTutto" object:nil];
}

- (void) ricaricaTutto {
    [self clearTable];
    NSString *rubrica = [[NSString alloc] initWithFormat:@"%@",((UIButton*)(self.headings)[selected]).currentTitle];
	[self loadTable];
	
	[self.dataAccess GetTeleTicinoData:rubrica];
    
    [self.loading startAnimating];
    [self.loadingLabel setHidden:NO];
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_moviePlayerView){
        self.moviePlayerView = nil;
    }
}

- (void) traverseElement:(TBXMLElement *)element {
    
    do {
        // Display the name of the element
        
        // Obtain first attribute from element
        //TBXMLAttribute * attribute = element->firstAttribute;
        
        NSString *elementName = [TBXML elementName:element];
        /*
        if ([elementName length]) {
            AppRecord *appRecord = [[AppRecord alloc] init];
            NSString *appID = [TBXML valueOfAttributeNamed:@"id" forElement:element];
            NSString *title = [TBXML valueOfAttributeNamed:@"title" forElement:element];
            NSString *imageURLString = [TBXML valueOfAttributeNamed:@"imageURLString" forElement:element];
            NSString *detailURLString = [TBXML valueOfAttributeNamed:@"detailURLString" forElement:element];
            
            NSLog(appID);
            NSLog(title);
            NSLog(imageURLString);
            NSLog(detailURLString);
            
            appRecord.appID = appID;
            appRecord.title = title;
            appRecord.imageURLString = imageURLString;
            appRecord.detailURLString = detailURLString;
            
            [arrayDati addObject:appRecord];
            [appRecord release];
        }
         */
        
        if ([elementName isEqualToString:@"news"]) {
            AppRecord *appRecord = [[AppRecord alloc] init];
            
            TBXMLElement *appID = [TBXML childElementNamed:@"blog" parentElement:element];
            TBXMLElement *pubDate = [TBXML childElementNamed:@"pubDate" parentElement:element];
            TBXMLElement *title = [TBXML childElementNamed:@"title" parentElement:element];
            TBXMLElement *subtitle = [TBXML childElementNamed:@"subtitle" parentElement:element];
            TBXMLElement *imageURLString = [TBXML childElementNamed:@"img" parentElement:element];
            TBXMLElement *detailURLString = [TBXML childElementNamed:@"link" parentElement:element];
            
            if (appID) appRecord.appID = [TBXML textForElement:appID];
            if (title) appRecord.title = [TBXML textForElement:title];
            if (subtitle) appRecord.subtitle = [TBXML textForElement:subtitle];
            if (pubDate) appRecord.pubDate = [TBXML textForElement:pubDate];
            if (imageURLString) appRecord.imageURLString = [TBXML textForElement:imageURLString];
            if (detailURLString) appRecord.detailURLString = [TBXML textForElement:detailURLString];
            
            [self.arrayDati addObject:appRecord];
        }
        
        // if the element has child elements, process them
        //if (element->firstChild) [self traverseElement:element->firstChild];
        
        // Obtain next sibling element
    } while ((element = element->nextSibling));
    
}

//#define ELEMENTI 8

-(void) ricaricaTabella:(NSMutableData *)data {
	self.arrayDati = [[NSMutableArray alloc] init];
    if (!data){
        [self.table setHidden:YES];
        [self.loading stopAnimating];
        [self.loadingLabel setHidden:YES];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Impossibile contattare il server, connettività limitata o assente. Verifica la tua connessione a Internet e riprova. Se il problema persiste riprova più tardi." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        return;
    }
    
    TBXML * tbxml = [TBXML tbxmlWithXMLData:data];
    
    if ((tbxml.rootXMLElement) && (tbxml.rootXMLElement->firstChild))
    {
        [self traverseElement:tbxml.rootXMLElement->firstChild];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Errore" message:@"Errore di rete" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    // release resources
    
    /*
	
	TBXML *tbxml = [[TBXML tbxmlWithXMLData:data] retain];
	
	NSMutableArray *temp = [Common XMLGetItemsBelowNodeName:tbxml.rootXMLElement NodeName:@"news"];
	
	[tbxml release];
	
	AppRecord *appRecord = nil;
	int j = 0;
	int x = 0;
    
    
	
	for (int i = 0; i < [temp count]; i++) {
		if (j == 0)	{
			++x;
			if ([arrayDati count] < x) {
				appRecord = [[AppRecord alloc] init];
			} else {
				appRecord = [arrayDati objectAtIndex: (x - 1)];
			}
		}
		if (j == 0) {
			appRecord.pubDate = ((ItemKeyValue *)[temp objectAtIndex:i]).nodeValue;
		}	
		if (j == 1) {
			appRecord.title = ((ItemKeyValue *)[temp objectAtIndex:i]).nodeValue;
		}	
		if (j == 2) {
			appRecord.subtitle = ((ItemKeyValue *)[temp objectAtIndex:i]).nodeValue;
		}	
		if (j == 3) {
			appRecord.imageURLString = ((ItemKeyValue *)[temp objectAtIndex:i]).nodeValue;
		}	
		if (j == 4) {
			appRecord.detailURLString = ((ItemKeyValue *)[temp objectAtIndex:i]).nodeValue;
		}
		
        if (j == 7) {
            appRecord.appID = ((ItemKeyValue *)[temp objectAtIndex:i]).nodeValue;
        }
		++j;
		if (j == ELEMENTI)	{
			if ([arrayDati count] < x) {
				[arrayDati addObject:appRecord];
				[appRecord release];
			}
			
			j = 0;
			
		}
	}
	
     */
    
    [self.table setContentOffset:CGPointZero animated:NO];
	[self.table setHidden:NO];
	[self.loading stopAnimating];
    [self.loadingLabel setHidden:YES];
	[self.table reloadData];
	
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateArrows];
}

#pragma mark -
#pragma mark Table view delegate

#define kLoadingTag 729

- (void)moviePlayerPlaybackStateDidChange:(id)anObject{
    for (UIView* aView in [_moviePlayerView.view subviews]){
        if (aView.tag == kLoadingTag && [aView isKindOfClass:[UIActivityIndicatorView class]]){
            [(UIActivityIndicatorView*)aView stopAnimating];
            break;
        }
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:_moviePlayerView.moviePlayer];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	//if (selected!=([headings count]-1)){
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopVideo" object:nil];
    
    AppRecord *appRecord = (self.arrayDati)[indexPath.row];

    self.detailView = [[Detail_UIViewController alloc] initWithNibName:@"Detail_UIViewController" bundle:nil];
    _detailView.articleNumber = [appRecord.appID intValue];
    UILabel *label = (UILabel *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:2];
    _detailView.myTitle = label.text;
    label = (UILabel *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:3];
    _detailView.mySubtitle = label.text;
    NSString *section = ((UIButton *)(self.headings)[selected]).titleLabel.text;
    _detailView.section = [NSString stringWithString:section];
    _detailView.factory = @"ticinonews";
    _detailView.articleNumber = [appRecord.appID intValue];
    
    [_detailView setDelegate:self.delegate];
    
    [self.delegate presentDetail:_detailView];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.0;
}

#define kCenterOffset 15


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if ([((UIButton *)self.headings[selected]).titleLabel.text isEqualToString:@"Servizi TG"])
        [[cell viewWithTag:20] setHidden:NO];
    
    return cell;
}

#pragma mark -
#pragma mark Table cell image support

-(void)rubricaTabButtonTouchUpInside:(id)sender {
	if (((UIButton*)sender).tag != selected) {
		[super rubricaTabButtonTouchUpInside:sender];	
		
		NSString *rubrica = [[NSString alloc] initWithFormat:@"%@",((UIButton*)(self.headings)[selected]).currentTitle];
		
		[self clearTable];
		[self loadTable];
		
		self.tabItemIsVisible = NO;
		
		[self.dataAccess GetTeleTicinoData:rubrica];
	}
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

