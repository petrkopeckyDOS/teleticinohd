//
//  Ticinonews_UIViewController.m
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Ticinonews_UIViewController.h"
#import "Common.h"
#import "TBXML.h"
#import "AppRecord.h"
#import "Info_UIViewController.h"
#import "Teleticino_iPadAppDelegate.h"
#import "HomeViewController.h"

/* 
 topnews ticino svizzera estero sport servizitg
*/

#define kTopnews	@"Topnews"
#define kTicino		@"Ticino"
#define kSvizzera	@"Svizzera"
#define kEstero		@"Estero"
#define kSport		@"Sport"
#define kServizitg	@"Servizi TG"

#define kTopnewsCellHeight 160
#define kNewsCellHeight 60

@interface Ticinonews_UIViewController ()

- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath;

@end

@implementation Ticinonews_UIViewController

- (BOOL)webView:(UIWebView*)mywebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType{
    if (UIWebViewNavigationTypeLinkClicked == navigationType){
        NSURL *url=[request URL];
        NSString *scheme = [[url scheme] lowercaseString];
        if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"]){
            [[UIApplication sharedApplication] openURL:url]; 
            return NO;
        }
        else {
            return YES;
        }
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    webView.hidden = NO;
    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('my_image').height;"];
    CGRect frame = CGRectMake(0, 361 + 50 - [height intValue], 320, [height intValue]);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [webView setFrame:frame];
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 365 - frame.size.height);
    [UIView commitAnimations];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    webView.hidden = YES;
    CGRect frame = CGRectMake(0, 411, 320, 50);
    [webView setFrame:frame];
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 365);
}


#pragma mark -
#pragma mark View lifecycle

- (void)ricarica {
    if (!_isLoading)
    {
        self.isLoading = YES;
        [self.table setHidden:YES];
        [self.loading startAnimating];
        
        [_arrayDati removeAllObjects];
        [self.imageDownloadsInProgress removeAllObjects];
        
        [_dataAccess SetItemsReceivedSelector:self :@selector(ricaricaTabella:)];
        [_dataAccess GetTeleTicinoData:kTopnews];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    StatSendOperation *stats = [[StatSendOperation alloc] init];
//    [stats sendStats];
    
    [self.view.layer setBorderColor:[UIColor colorWithRed:190.0/255.0 green:190.0/255.0 blue:190.0/255.0 alpha:1.0].CGColor];
    [self.view.layer setBorderWidth:1.0];
    
    UIButton* infoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setLeftBarButtonItem:modalButton animated:YES];

	self.arrayDati = [[NSMutableArray alloc] init];
	self.dataAccess = [[DataAccess alloc] init];
	self.rubriche = @[kTicino, kSvizzera, kEstero, kSport, kServizitg];
    self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
	self.isLoading = NO;
    [self ricarica];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ricarica) name:@"ricaricaTutto" object:nil];
}

- (void)infoButtonAction:(id)sender{
    Info_UIViewController* info =[[Info_UIViewController alloc] initWithNibName:@"Info_UIViewController" bundle:nil];
    info.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.tabBarController presentModalViewController:info animated:YES];
}

- (void) traverseElement:(TBXMLElement *)element {
    
    do {
        // Display the name of the element
        
        // Obtain first attribute from element
        //TBXMLAttribute * attribute = element->firstAttribute;
        
        NSString *elementName = [TBXML elementName:element];
        
        if ([elementName isEqualToString:@"news"]) {
            AppRecord *appRecord = [[AppRecord alloc] init];
            
            TBXMLElement *appID = [TBXML childElementNamed:@"blog" parentElement:element];
            TBXMLElement *pubDate = [TBXML childElementNamed:@"pubDate" parentElement:element];
            TBXMLElement *title = [TBXML childElementNamed:@"title" parentElement:element];
            TBXMLElement *subtitle = [TBXML childElementNamed:@"subtitle" parentElement:element];
            TBXMLElement *imageURLString = [TBXML childElementNamed:@"img" parentElement:element];
            

            
            TBXMLElement *detailURLString = [TBXML childElementNamed:@"link" parentElement:element];
            
            if (appID) appRecord.appID = [TBXML textForElement:appID];
            if (title) appRecord.title = [TBXML textForElement:title];
            if (subtitle) appRecord.subtitle = [TBXML textForElement:subtitle];
            if (pubDate) appRecord.pubDate = [TBXML textForElement:pubDate];
            if (imageURLString) appRecord.imageURLString = [TBXML textForElement:imageURLString];
            if (detailURLString) appRecord.detailURLString = [TBXML textForElement:detailURLString];

            if ([appRecord.imageURLString length] > 0 ) {//string not empty
                NSString *textToCut = @"_rettverticale";
                NSRange cutLocation = [appRecord.imageURLString rangeOfString:textToCut];
                if (cutLocation.location > 0){
                    NSString *newImageUrl = [[NSString alloc] initWithFormat:@"%@_header.jpg",[appRecord.imageURLString substringToIndex:cutLocation.location]];
                    //[appRecord.imageURLString release];
                    appRecord.imageURLString = [newImageUrl copy];
                }
            }
            
            [_arrayDati addObject:appRecord];
        }
        
        // if the element has child elements, process them
        //if (element->firstChild) [self traverseElement:element->firstChild];
        
        // Obtain next sibling element
    } while ((element = element->nextSibling));
    
}

//#define ELEMENTI 7
-(void) ricaricaTabella:(NSMutableData *)data {
	
    if (!data){
        [self.table setHidden:YES];
        [self.loading stopAnimating];
        self.isLoading = NO;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Attenzione" message:@"Impossibile contattare il server, connettività limitata o assente. Verifica la tua connessione a Internet e riprova. Se il problema persiste riprova più tardi." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        return;
    }
	NSLog(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
	
	TBXML * tbxml = [TBXML tbxmlWithXMLData:data];
    
    if (tbxml.rootXMLElement && (tbxml.rootXMLElement->firstChild))
    {
        [self traverseElement:tbxml.rootXMLElement->firstChild];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Errore" message:@"Errore di rete" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
        [self.navigationController popViewControllerAnimated:YES];
    }	
	NSLog(@"%d",[_arrayDati count]);
	
	
	[self.table setHidden:NO];
	[self.loading stopAnimating];
    self.isLoading = NO;
	[self.table reloadData];
	
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, 365);
}

/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 230;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"CellNews";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	// add a placeholder cell while waiting on table data

	
    if (cell == nil) {
		if (indexPath.row != 0) { 
			[[NSBundle mainBundle] loadNibNamed:@"RubricaCell" owner:self options:nil];
			cell = _miaCell;
			self.miaCell = nil;
		} else {
			[[NSBundle mainBundle] loadNibNamed:@"TopNewsCell" owner:self options:nil];
			cell = _topCell;
			self.topCell = nil;			
		}
    }
	
	int nodeCount = [self.arrayDati count];
	// Leave cells empty if there's no data yet
    if (nodeCount > 0) {
		
		if (!indexPath.row) {
			// Set up the cell...
			AppRecord *appRecord = (self.arrayDati)[indexPath.row];
        
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		
			[dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm:ss"];
			NSDate *pubDate = [dateFormatter dateFromString: appRecord.pubDate];
		
			[dateFormatter setDateFormat:@"dd.MM HH:mm"];
		
		
			UILabel *label = (UILabel *)[cell viewWithTag:1];
			[label setText: [NSString stringWithFormat: @"%@",[dateFormatter stringFromDate:pubDate]]];	
            
            NSMutableAttributedString *news = [[NSMutableAttributedString alloc] init];
            NSAttributedString *title = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", appRecord.title] attributes:@{NSForegroundColorAttributeName: [UIColor redColor],
                                                                                                                                                     NSFontAttributeName: [UIFont boldSystemFontOfSize:22.0]}];
            [news appendAttributedString:title];
            
            NSAttributedString *subtitle = [[NSAttributedString alloc] initWithString:appRecord.subtitle attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                                                                      NSFontAttributeName: [UIFont systemFontOfSize:18.0]}];
            [news appendAttributedString:subtitle];
            
			label = (UILabel *)[cell viewWithTag:2];
			[label setAttributedText:news];
			
			// Only load cached images; defer new downloads until scrolling ends
			if (!appRecord.appIcon){
				if (self.table.dragging == NO && self.table.decelerating == NO){
					[self startIconDownload:appRecord forIndexPath:indexPath];
				}
				// if a download is deferred or in progress, return a placeholder image
				UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
                [imageView setContentMode:UIViewContentModeCenter];
				imageView.image = [UIImage imageNamed:@"Placeholder"];
			}
			else{
				UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
                [imageView setContentMode:UIViewContentModeScaleAspectFill];
				imageView.image = appRecord.appIcon;            
			}
		}
		else {
			UILabel *label = (UILabel *)[cell viewWithTag:2];
			[label setText: _rubriche[(indexPath.row-1)]];
		}
    }
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row) {
		self.news = [[NewsTab_UIViewController alloc] initWithNibName:@"NewsTab_UIViewController" bundle:nil];
		_news.selected = indexPath.row - 1;
		for (NSString *section in _rubriche){
			[_news addHeading:section];
			NSLog(@"%@",section);
		}
		[self.navigationController pushViewController:_news animated:YES];
	} else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopVideo" object:nil];

        AppRecord *appRecord = _arrayDati[indexPath.row];
        
		Detail_UIViewController *detail = [[Detail_UIViewController alloc] initWithNibName:@"Detail_UIViewController" bundle:nil];
		detail.section = @"TopnewsDetail";
		UILabel *label = (UILabel *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:2];
		detail.myTitle = label.text;
		detail.factory = @"ticinonews";
		label = (UILabel *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:3];
		detail.mySubtitle = label.text;
        detail.articleNumber = [appRecord.appID intValue];
		[self.navigationController pushViewController:detail animated:YES];
        [detail setDelegate:self.delegate];
 
        
        [detail setModalTransitionStyle:UIModalTransitionStylePartialCurl];
        [self.delegate presentDetail:detail];
    }
}

#pragma mark -
#pragma mark Table cell image support

- (void)startIconDownload:(AppRecord *)appRecord forIndexPath:(NSIndexPath *)indexPath{
    IconDownloader *iconDownloader = _imageDownloadsInProgress[indexPath];
    if (iconDownloader == nil){
        iconDownloader = [[IconDownloader alloc] init];
        iconDownloader.appRecord = appRecord;
        iconDownloader.indexPathInTableView = indexPath;
        iconDownloader.delegate = self;
        _imageDownloadsInProgress[indexPath] = iconDownloader;
		[iconDownloader proportionalImageWithWidth:400];
        [iconDownloader startDownload];
	}
}

// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
- (void)loadImagesForOnscreenRows
{
    if ([self.arrayDati count] > 0)
    {
        NSArray *visiblePaths = [self.table indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
			if (indexPath.row == 0) {
				AppRecord *appRecord = (self.arrayDati)[indexPath.row];
				if (!appRecord.appIcon){ // avoid the app icon download if the app already has an icon
					[self startIconDownload:appRecord forIndexPath:indexPath];
				}
			}
        }
    }
}

// called by our ImageDownloader when an icon is ready to be displayed
- (void)appImageDidLoad:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = _imageDownloadsInProgress[indexPath];
    if (iconDownloader != nil)
    {
        UITableViewCell *cell = [self.table cellForRowAtIndexPath:iconDownloader.indexPathInTableView];
        
        // Display the newly loaded image
		UIImageView *imageView = (UIImageView *)[cell viewWithTag:4];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
		imageView.image =iconDownloader.appRecord.appIcon;
        
        
    }
}

#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)

// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // terminate all pending download connections
    NSArray *allDownloads = [self.imageDownloadsInProgress allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
}

@end

