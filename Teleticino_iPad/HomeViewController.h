//
//  Teleticino_iPadViewController.h
//  Teleticino_iPad
//
//  Created by Davide Vincenzi on 20.04.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ticinonews_UIViewController.h"
#import "GAITrackedViewController.h"

@class Detail_UIViewController;

@interface HomeViewController : GAITrackedViewController <DetailViewDelegate> {

}

@property (nonatomic, strong) UIPopoverController *popoverCont;
@property (nonatomic, weak) IBOutlet UIView *ticinoNewsView;
@property (nonatomic, weak) IBOutlet UIView *topView;
@property (nonatomic, weak) IBOutlet UIView *bottomView;
@property (nonatomic, weak) IBOutlet UIView *pubblicitaView;
@property (nonatomic, strong) UIView *volumeView;
@property (nonatomic, unsafe_unretained) BOOL initialized;

- (void) setupViewWithOrientation;
- (IBAction) ricaricaTutto;
- (IBAction) showInfo:(UIBarButtonItem *)sender;

@end
