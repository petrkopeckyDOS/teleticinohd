//
//  Palinsesto_UIViewController.h
//  TeleTicino
//
//  Created by Mariano Pirelli on 03.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataAccess.h"


@interface Palinsesto_UIViewController : UIViewController <UIWebViewDelegate> {

}

@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, strong) DataAccess *dataAccess;
@property (nonatomic, strong) NSMutableArray *arrayDati;
@property (nonatomic, strong) NSMutableArray *arraySettimana;
@property (nonatomic, weak) IBOutlet UITableViewCell *miaCell;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loading;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segment;

- (void)caricaSettimana:(TBXML *)tbxml;

@end
